
/// <summary>
/// Esto es un informe para Listar los Productos que estamos haciendo con Carlos 
/// </summary>
report 50000 "MXCListadoProductosC01"
{
    AdditionalSearchTerms = 'listado de Productos Ventas/Compras';
    ApplicationArea = All;
    Caption = 'Listado productos Ventas/Compras';
    //DefaultLayout se indica el informe quieres imprimir 
    DefaultLayout = RDLC;
    RDLCLayout = './Layout/listadoproductos.rdlc';
    UsageCategory = ReportsAndAnalysis;
    // ExcelLayoutMultipleDataSheets = true;

    dataset
    {
        dataitem(Productos; Item)  //dataitem(Nombre dataitm ; Nombre de la tabla)

        {
            PrintOnlyIfDetail = true;
            RequestFilterFields = "No.", "Item Category Code"; //campos que voy a filtrar en nuestro informe de los que van a 
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }
            column(No_Productos; "No.")
            {
            }
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                //DataItemLink = "No." = field("No.");//ESpecificamos que campo queremos lincar(Esta es una de las maneras que podemos usar)
                RequestFilterFields = "Posting Date";
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(DocumentNo_FacturaVenta; "Document No.")
                {
                }
                column(NoCliente; rCustomer."No.")
                {

                }
                column(NombreCliente; rCustomer.Name)
                {

                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }
                column(Quantity_FacturaVenta;
                Quantity)
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                //----otra forma de linkar el data set---//
                trigger OnPreDataItem()
                begin
                    //por propiedades
                    FacturaVenta.SetRange("No.", Productos."No.");
                end;
                //--------------------------//
                trigger OnAfterGetRecord()//aplicar filtros para conseguir datos.los OnAfterGetRecord()No tinen parametro --pasa siempre
                begin
                    //si no existe el cliente ,limpia de memoria ese registro
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        //con esta forma limpia
                        Clear(rCustomer);
                    end;

                    //aplianccdo un rango (si no le pongo hasta +" '&'"+ ) y aplicando filtros  
                    //setRange(donde quiero que apliques el rango, de Donde quiero sacar el rango)
                    // rCustomer.SetRange("No.", FacturasVenta."Sell-to Customer No.");
                    // rCustomer.SetFilter("No.", FacturasVenta."Sell-to Customer No.");
                    // rCustomer.FindFirst();

                end;

            }
            dataitem(FacturaCompra; "Purch. Inv. Line")
            {
                RequestFilterFields = "Posting Date";
                column(AmountIncludingVAT_FacturasCompra; "Amount Including VAT")
                {
                }
                column(DocumentNo_FacturaCompra; "Document No.")
                {
                }

                column(NombreProveedor; rVendor.Name)
                {
                }
                column(NoProveedor; rVendor."No.")
                {
                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }
                column(Quantity_FacturasCompra; Quantity)
                {
                }
                column(UnitCost_FacturasCompra; "Unit Cost")
                {
                }
                trigger OnPreDataItem()
                begin
                    FacturaCompra.SetRange("No.", Productos."No."); //linkamos el 
                end;

                trigger OnAfterGetRecord()
                begin
                    if not rVendor.get(FacturaCompra."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;
                end;
            }
            trigger OnAfterGetRecord()

            var
                rSalesInvoiceLine: Record "Sales Invoice Line";
            begin
                rSalesInvoiceLine.SetRange("No.", Productos."No.");
                if rSalesInvoiceLine.IsEmpty then begin
                    CurrReport.Skip();
                end;
            end;
        }
    }
    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
}
