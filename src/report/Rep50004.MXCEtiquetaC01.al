/// <summary>
/// VAmos hacer las etiquetas 
/// </summary>
report 50004 "MXCEtiquetaC01"
{
    Caption = 'Etiqueta';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './Layout/Etiqueta.rdlc';
    UseRequestPage = false;


    dataset
    {
        dataitem("Sales Shipment Line"; "Sales Shipment Line")
        {

            column(No_SalesShipmentLine; "No.")
            {
            }
            column(Description_SalesShipmentLine; Description)
            {
            }
            column(DocumentNo_SalesShipmentLine; "Document No.")
            {
            }
            //saque una etiqueta por linea de pedido
            dataitem(Integer; Integer)
            {

                column(Number_Integer; Number)
                {
                }
                trigger OnPreDataItem()
                begin
                    Integer.SetRange(Number, 1, "Sales Shipment Line".Quantity);
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }
    }
}