/// <summary>
/// Report "ListadoClientesProveedores" (ID 50105).
/// </summary>
report 50005 "MXCListadoClientesProveedores"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    //DefaultLayout se indica el informe quieres imprimir 
    DefaultLayout = RDLC;
    RDLCLayout = './Layout/listadoClientesProveedores.rdlc';
    AdditionalSearchTerms = 'listado Clientes Proveedores';
    Caption = 'listado de Clientes/Proveedores';

    dataset
    {
        dataitem(Customer; Customer)
        {
            RequestFilterFields = "No.", "Bill-to Customer No.";

            column(Bill_to_Customer_No_; "Bill-to Customer No.")
            {

            }
            column(No_Customer; "No.")
            {
            }
            column(Name_Customer; Name)
            {
            }
            column(NameCompanyInformation; rCompanyInformation.Name)
            {

            }
            column(LogoCompanyInformation; rCompanyInformation.Picture)
            {

            }

            dataitem("Sales Invoice Line"; "Sales Invoice Line")
            {
                DataItemLink = "No." = field("No."), "Document No." = field("Bill-to Customer No.");
                RequestFilterFields = "Posting Date";
                column(DocumentNo_SalesInvoiceLine; "Document No.")
                {
                }
                column(PostingDate_SalesInvoiceLine; "Posting Date")
                {
                }
                column(Quantity_SalesInvoiceLine; Quantity)
                {
                }
                column(UnitPrice_SalesInvoiceLine; "Unit Price")
                {
                }
                column(AmountIncludingVAT_SalesInvoiceLine; "Amount Including VAT")
                {
                }
                trigger OnAfterGetRecord()//aplicar filtros para conseguir datos.los OnAfterGetRecord()No tinen parametro --pasa siempre
                begin
                    //si no existe el cliente ,limpia de memoria ese registro
                    if not rCustomer.Get("Sales Invoice Line"."Sell-to Customer No.") then begin
                        //con esta forma limpia
                        Clear(rCustomer);
                    end;
                end;
            }

        }
        //Lineas de Proveedores
        dataitem(Vendor; Vendor)

        {
            RequestFilterFields = "No.";
            column(No_Vendor; "No.")
            {
            }
            column(Name_Vendor; Name)
            {
            }
            column(Address_Vendor; Address)
            {
            }


            dataitem("Purch. Inv. Line"; "Purch. Inv. Line")
            {
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "No.", "Posting Date";

                column(DocumentNo_PurchInvLine;
                "Document No.")
                {
                }
                column(No_PurchInvLine; "No.")
                {
                }
                column(PostingDate_PurchInvLine; "Posting Date")
                {
                }
                column(UnitCost_PurchInvLine; "Unit Cost")
                {
                }
                column(Quantity_PurchInvLine; Quantity)
                {
                }
                column(AmountIncludingVAT_PurchInvLine; "Amount Including VAT")
                {
                }
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }
    }
    trigger OnPreReport()
    begin
        if not rCompanyInformation.get() then begin
            Clear(rCompanyInformation);
            rCompanyInformation.CalcFields(Picture);
        end;
        //para sacar el Logo de la empresa
        rCompanyInformation.CalcFields(Picture);
    end;
    //variable global para sacar el nombre de la empresa
    var
        rCompanyInformation: Record "Company Information";
        rCustomer: Record Customer;
}