/// <summary>
/// otro informe con carlos 
/// </summary>
report 50002 "MXCFacturaVentaC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;

    DefaultLayout = RDLC;
    RDLCLayout = './Layout/facturasVentaMxc.rdlc';
    AdditionalSearchTerms = 'FacturasVentaMXC';
    Caption = 'FacturasVentaMXC';

    dataset
    {
        dataitem("Sales Invoice Header"; "Sales Invoice Header")
        {

            column(No_SalesInvoiceHeader; "No.")
            {
            }
            column(xCustAddr1; xCustAddr[1])
            {
            }
            column(xCustAddr2; xCustAddr[2])
            {
            }
            column(xShipAddr; xShipAddr[1])
            {

            }
            column(xShipAddr2; xShipAddr[2])
            {

            }
            dataitem(Integer; Integer)
            {



                dataitem("Sales Invoice Line"; "Sales Invoice Line")
                {


                    column(LineNo_SalesInvoiceLine; "Line No.")
                    {
                    }

                    trigger OnAfterGetRecord()
                    begin
                        "Sales Invoice Line".SetRange("Document No.", "Sales Invoice Header"."No.");
                    end;
                }
            }
            trigger OnAfterGetRecord()
            begin
                //
                cuFormatAdress.SalesInvSellTo(xCustAddr, "Sales Invoice Header");
                //
                cuFormatAdress.SalesInvShipTo(xShipAddr, xCustAddr, "Sales Invoice Header");
                rCompanyInfo.get;
                //cuFormatAdress.Company(xCompannyAddr, rCompanyInfo);


            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(xcopias; xcopias)
                    {
                        ApplicationArea = all;

                    }
                }
            }
        }

        actions
        {
            area(processing)
            {

            }
        }
    }

    var
        rCompanyInfo: Record "Company Information";
        cuFormatAdress: Codeunit "Format Address";
        xcopias: Integer;
        //variables globales
        //declaracon de un array
        xCustAddr: array[8] of Text;
        xShipAddr: array[8] of Text;

}