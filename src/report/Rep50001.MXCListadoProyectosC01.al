/// <summary>
/// Report MXCListadoProyectosC01 (ID 50101).
/// </summary>
report 50001 "MXCListadoProyectosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './Layout/listadoproyectos.rdlc';//indicas de que tipo va ser el informe
    AdditionalSearchTerms = 'Listado proyectos';
    Caption = 'Listado proyectos';

    dataset
    {
        dataitem(Proyectos; Job)
        {

            // DataItemTableView=sorting("No.");
            RequestFilterFields = "No.", "Creation Date", "Status";
            column(No_Proyectos; "No.")
            {
            }
            //a que cliente va
            column(BilltoCustomerNo_Proyectos; "Bill-to Customer No.")
            {
            }
            column(BilltoName_Proyectos; "Bill-to Name")
            {
            }

            column(Description_Proyectos; Description)
            {
            }
            column(CreationDate_Proyectos; Format("Creation Date"))
            {
            }
            column(Status_Proyectos; Status)
            {
            }
            /////DESCRIPCIONES////
            column(DC_No_Proyectos; FieldCaption("No."))
            {
            }
            column(DC_BilltoCustomerNo_Proyectos; FieldCaption("Bill-to Customer No."))
            {
            }
            column(DC_BilltoName_Proyectos; FieldCaption("Bill-to Name"))
            {
            }
            column(DC_Description_Proyectos; FieldCaption(Description))
            {
            }
            column(DC_CreationDate_Proyectos; FieldCaption("Creation Date"))
            {
            }
            column(DC_Status_Proyectos; FieldCaption(Status))
            {
            }
            ///////////FIN_DESCRIPCION//////////////
            column(rXcolor; rXcolor)
            {
            }

            dataitem("Job Task"; "Job Task")
            {
                RequestFilterFields = "Job Task No.", "Job Task Type";
                //este campo se enlaza con el campo no del proyecto
                DataItemLink = "Job No." = field("No.");

                column(JobTaskNo_JobTask; "Job Task No.")
                {
                }
                column(JobTaskType_JobTask; "Job Task Type")
                {
                }
                column(Description_JobTask; Description)
                {
                }
                column(StartDate_JobTask; Format("Start Date"))
                {
                }
                column(EndDate_JobTask; Format("End Date"))
                {
                }
                /////DESCRIPCIONES////
                column(DC_JobTaskNo_JobTask; FieldCaption("Job Task No."))
                {
                }
                column(DC_JobTaskType_JobTask; FieldCaption("Job Task Type"))
                {
                }
                column(DC_Description_JobTask; FieldCaption(Description))
                {
                }
                column(DC_StartDate_JobTask; FieldCaption("Start Date"))
                {
                }

                column(DC_EndDate_JobTask; FieldCaption("End Date"))
                {
                }
                ///////////FIN_DESCRIPCION//////////////
                dataitem("Job Planning Line"; "Job Planning Line")
                {

                    RequestFilterFields = "No.", "Planning Date", Type;
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");
                    // DataItemLink="Job No."=field()

                    column(No_JobPlanningLine; "No.")
                    {
                    }
                    column(Description_JobPlanningLine; Description)
                    {
                    }
                    column(Quantity_JobPlanningLine; Quantity)
                    {
                    }
                    //se una para analizar las horas de un recurso,para aumentar
                    column(PlanningDate_JobPlanningLine; Format("Planning Date"))
                    {

                    }
                    column(LineAmount_JobPlanningLine; "Line Amount")
                    {
                    }
                    column(Type_JobPlanningLine; "Type")
                    {
                    }
                    column(LineNo_JobPlanningLine; "Line No.")
                    {
                    }
                    /////DESCRIPCIONES////
                    column(DC_No_JobPlanningLine; FieldCaption("No."))
                    {
                    }
                    column(DC_Description_JobPlanningLine; FieldCaption(Description))
                    {
                    }
                    column(DC_Quantity_JobPlanningLine; FieldCaption(Quantity))
                    {
                    }
                    column(DC_PlanningDate_JobPlanningLine; FieldCaption("Planning Date"))
                    {
                    }

                    column(DC_LineAmount_JobPlanningLine; FieldCaption("Line Amount"))
                    {
                    }

                    column(DC_Type_JobPlanningLine; FieldCaption("Type"))
                    {
                    }
                    column(DC_LineNo_JobPlanningLine; FieldCaption("Line No."))
                    {
                    }
                    ///////////FIN_DESCRIPCION//////////////


                    dataitem("Job Ledger Entry"; "Job Ledger Entry")
                    {
                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");


                        column(DocumentNo_JobLedgerEntry; "Document No.")
                        {
                        }
                        column(DocumentDate_JobLedgerEntry; "Document Date")
                        {
                        }
                        column(Type_JobLedgerEntry; "Type")
                        {
                        }
                        column(EntryType_JobLedgerEntry; "Entry Type")
                        {
                        }

                        column(LineType_JobLedgerEntry; "Line Type")
                        {
                        }
                        column(WorkTypeCode_JobLedgerEntry; "Work Type Code")
                        {
                        }
                        column(TransactionType_JobLedgerEntry; "Transaction Type")
                        {
                        }
                        column(LedgerEntryType_JobLedgerEntry; "Ledger Entry Type")
                        {
                        }
                        /////DESCRIPCIONES////
                        column(DC_DocumentNo_JobLedgerEntry; FieldCaption("Document No."))
                        {
                        }
                        column(DC_DocumentDate_JobLedgerEntry; FieldCaption("Document Date"))
                        {
                        }
                        column(DC_Type_JobLedgerEntry; FieldCaption("Type"))
                        {

                        }
                        column(DC_EntryType_JobLedgerEntry; FieldCaption("Entry Type"))
                        {
                        }

                        column(DC_LineType_JobLedgerEntry; FieldCaption("Line Type"))
                        {
                        }


                        column(DC_WorkTypeCode_JobLedgerEntry; FieldCaption("Work Type Code"))
                        {
                        }
                        column(DC_TransactionType_JobLedgerEntry; FieldCaption("Transaction Type"))
                        {
                        }

                        column(DC_LedgerEntryType_JobLedgerEntry; FieldCaption("Ledger Entry Type"))
                        {
                        }

                        ///////////FIN_DESCRIPCION//////////////
                    }
                }
            }
            //linea del proyecto dependiendo del estado del proyecto
            trigger OnAfterGetRecord()
            begin

                case
                Proyectos.Status of
                    Proyectos.Status::Completed:
                        //verde
                        begin
                            rXcolor := '#59F52E'
                        end;
                    Proyectos.Status::Open:
                        begin
                            //amarillo
                            rXcolor := '#F5C22E'
                        end;
                    Proyectos.Status::Planning:
                        begin
                            //celeste
                            rXcolor := '#2ECDF5'
                        end;
                    Proyectos.Status::Quote:
                        begin
                            //rojo
                            rXcolor := '#F5462E'
                        end;


                end;
            end;


        }





    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    // field(Name; SourceExpression)
                    // {
                    //     ApplicationArea = All;

                    // }
                }
            }
        }

    }
    //variables globales

    var
        rXcolor: Text;
}