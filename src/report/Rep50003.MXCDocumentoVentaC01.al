/// <summary>
/// ahora un documento con Carlos
/// 
/// </summary>
report 50003 "MXCDocumentoVentaC01"
{

    UsageCategory = Administration;
    ApplicationArea = All;
    // DefaultLayout = RDLC;
    // RDLCLayout = './Layout/documentoVentaMxc.rdlc';
    DefaultLayout = Word;
    WordLayout = './Layout/documentoVentaMxc.docx';
    AdditionalSearchTerms = 'DocumentoVentaMXC';
    Caption = 'Documento de VentaMXC';

    dataset
    {
        dataitem(SalesHeader; "Sales Header")
        {

            RequestFilterFields = "No.", "Document Type";
            column(No_SalesHeader; "No.")
            {
            }
            column(PostingDate_SalesHeader; "Posting Date")
            {
            }
            column(DocumentType_SalesHeader; "Document Type")
            {
            }
            column(SelltoCustomerNo_SalesHeader; "Sell-to Customer No.")
            {
            }
            column(xDocumentLaber; xDocumentLaber)
            {

            }
            column(xCompanyAddr1; xCompanyAddr[1])
            {

            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {

            }

            column(xCompanyAddr3; xCompanyAddr[3])
            {

            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {

            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {

            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {

            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {

            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {

            }
            column(xCustAddr1; xCustAddr[1])
            {

            }
            column(xCustAddr2; xCustAddr[2])
            {

            }

            column(xCustAddr3; xCustAddr[3])
            {

            }
            column(xCustAddr4; xCustAddr[4])
            {

            }
            column(xCustAddr5; xCustAddr[5])
            {

            }
            column(xCustAddr6; xCustAddr[6])
            {

            }
            column(xCustAddr7; xCustAddr[7])
            {

            }
            column(xCustAddr8; xCustAddr[8])
            {

            }
            column(Logo; rCompanyInfo.Picture)
            {

            }
            column(rXcolor; rXcolor)
            {
            }
            column(xTotalIVA11; xTotalIVA[1, 1])
            {

            }
            column(xTotalIVA21; xTotalIVA[2, 1])
            {

            }
            column(xTotalIVA31; xTotalIVA[3, 1])
            {

            }
            column(xTotalIVA41; xTotalIVA[4, 1])
            {

            }
            /////con [x,2]
            column(xTotalIVA12; xTotalIVA[1, 2])
            {

            }
            column(xTotalIVA22; xTotalIVA[2, 2])
            {

            }
            column(xTotalIVA32; xTotalIVA[3, 2])
            {

            }
            column(xTotalIVA42; xTotalIVA[4, 2])
            {

            }
            /////con [x,3]
            column(xTotalIVA13; xTotalIVA[1, 3])
            {

            }
            column(xTotalIVA23; xTotalIVA[2, 3])
            {

            }
            column(xTotalIVA33; xTotalIVA[3, 3])
            {

            }
            column(xTotalIVA43; xTotalIVA[4, 3])
            {

            }

            /////con [x,4]
            column(xTotalIVA14; xTotalIVA[1, 2])
            {

            }
            column(xTotalIVA24; xTotalIVA[2, 4])
            {

            }
            column(xTotalIVA34; xTotalIVA[3, 4])
            {

            }
            column(xTotalIVA44; xTotalIVA[4, 4])
            {

            }

            dataitem(Copias; Integer)
            {


                column(Number_Copias; Number)
                {
                }

                dataitem("Sales Line"; "Sales Line")
                {

                    //DataItemLink = "Document No." = field("No."), "Document Type" = field("Document Type");

                    column(Quantity_SalesLine; Quantity)
                    {
                    }
                    column(Description_SalesLine; Description)
                    {
                    }
                    column(No_SalesLine; "No.")
                    {
                    }
                    // trigger OnPreDataItem()
                    // begin
                    //     "Sales Line".SetRange("Document No.", SalesHeader."No.");
                    //     "Sales Line".SetRange("Document Type", SalesHeader."Document Type");
                    // end;

                    dataitem("Extended Text Line"; "Extended Text Line")
                    {
                        DataItemLink = "No." = field("No.");
                        column(text_ExtendedTextLine; Text)
                        {

                        }

                    }
                    // dataitem("Item Cross Reference"; "Item Cross Reference")
                    // {

                    //     DataItemLink = "Item No." = field("No.");
                    //     //
                    //     DataItemTableView = where("Cross-Reference type" = const(Customer));

                    //     column(CrossReferenceNo_ItemCrossReference; "Cross-Reference No.")
                    //     {
                    //     }
                    //     column(Description_ItemCrossReference; Description)
                    //     {
                    //     }
                    // }
                    trigger OnPreDataItem()
                    begin
                        "Sales Line".SetRange("Document No.", SalesHeader."No.");
                        "Sales Line".SetRange("Document Type", SalesHeader."Document Type");
                    end;

                }
                trigger OnPreDataItem()
                begin
                    if xCopias = 0 then begin
                        xCopias := 1;
                    end;
                    Copias.SetRange(Number, 1, xCopias);
                end;



            }

            trigger OnAfterGetRecord()

            begin
                case SalesHeader."Document Type" of
                    salesheader."Document Type"::Quote:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLaber := 'Nº Oferta';
                            //azul1
                            rXcolor := '#0172F1';


                        end;
                    SalesHeader."Document Type"::Order:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLaber := 'Nº Pedido';

                            rXcolor := '#F1DD0C';
                        end;
                    SalesHeader."Document Type"::Invoice:
                        begin
                            cuFormatAddress.SalesHeaderSellTo(xCustAddr, SalesHeader);
                            xDocumentLaber := 'Nº Factura';
                            rXcolor := '#1AEB09';
                        end;

                end;
                CalculodDeIvaF();
            end;

            trigger OnPreDataItem()
            begin
                SalesHeader.SetRange("Document Type", xSalesHeader."Document Type");
                SalesHeader.SetFilter("No.", xDocumentNo);
            end;
        }


    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {
                    field(rSalesHeader;
                    xSalesHeader."Document Type")
                    {
                        ApplicationArea = All;

                    }
                    field(xDocumentNo;
                    xDocumentNo)
                    {
                        Caption = 'xDocumento ';
                        ApplicationArea = all;
                        //DataClassification = ToBeClassified;

                        trigger OnLookup(var xDocNo: Text):
                    Boolean
                        //onvalidate cuando se rellena 
                        //on loock una mira muy parcido a drill dow

                        var
                            rlSalesHeader: Page "Sales List";
                        begin
                            xSalesHeader.SetRange("Document Type", xSalesHeader."Document Type");
                            //saacar una pagina filtada por los cmapos que queremos.Aplico diltros de tabla a pagina
                            rlSalesHeader.SetTableView(xSalesHeader);
                            rlSalesHeader.LookupMode := true;///pongo la pagina en modo looup
                            if rlSalesHeader.RunModal() = Action::LookupOK then begin
                                //los registos que ha seleccionado 
                                rlSalesHeader.SetSelectionFilter(xSalesHeader);
                                //aplico sellecione de usuario de la pagina a la tabla
                                //bucle tipoco
                                if xSalesHeader.FindSet() then begin //busqueda 
                                                                     //bluque repetea
                                    repeat
                                        xDocumentNo += xSalesHeader."No." + '|';//elaboracionde filtros
                                    until xSalesHeader.Next() = 0;
                                    //hasta que los registos sean un 0

                                end;
                                xDocumentNo := DelChr(xDocumentNo, '>', '|');//limpio la concatenacion final
                            end;

                        end;
                    }
                    field(Copias1; xCopias)
                    {
                        ApplicationArea = all;
                    }
                }

            }
        }
    }

    //los triger al informe entero se hacen
    trigger OnPreReport()
    begin
        //cargar los datos y se saben  las claves 
        rCompanyInfo.Get();
        //calcula los campos calculados ,trae 
        // 
        rCompanyInfo.CalcFields(Picture);
        //usamos la code uni para que seté ,estas funciones mete 
        cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);

    end;


    //funcion global al 
    /// <summary>
    /// funcion que devulvera el calculo de IVa
    /// </summary>

    procedure CalculodDeIvaF()
    //variables locales a la funcion
    //insertar no gusrada nada en sql
    var
        rlSalesLine: Record "Sales Line";
        rlTempVATAMOUNT: Record "VAT Amount Line" temporary;
        i: Integer;

    begin
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        ;
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        //si me encuentras el conjunto de datos 
        if rlSalesLine.FindSet() then begin
            repeat
                //dos casos
                //primero pregunto si tiene el iva de eso..iva nacional 
                rlTempVATAMOUNT.SetRange("VAT Identifier", rlSalesLine."VAT Identifier");

                if rlTempVATAMOUNT.FindFirst() then begin
                    // rlTempVATAMOUNT.Init();
                    rlTempVATAMOUNT."VAT Base" += rlSalesLine.Amount;
                    rlTempVATAMOUNT."VAT %" := rlSalesLine."VAT %";
                    //cuanto porcentaje de iva
                    rlTempVATAMOUNT."VAT Amount" += rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAMOUNT."Amount Including VAT" += rlSalesLine."Amount Including VAT";
                    rlTempVATAMOUNT."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATAMOUNT.Modify(false);
                end else begin
                    rlTempVATAMOUNT.Init();
                    //cuanta base
                    rlTempVATAMOUNT."VAT Base" := rlSalesLine.Amount;
                    rlTempVATAMOUNT."VAT %" := rlSalesLine."VAT %";
                    //cuanto porcentaje de iva
                    rlTempVATAMOUNT."VAT Amount" := rlSalesLine."Amount Including VAT" - rlSalesLine.Amount;
                    rlTempVATAMOUNT."Amount Including VAT" := rlSalesLine."Amount Including VAT";
                    rlTempVATAMOUNT."VAT Identifier" := rlSalesLine."VAT Identifier";
                    rlTempVATAMOUNT.Insert(false);
                end;

            until rlSalesLine.Next() = 0;
        end;

        Clear(xTotalIVA);
        //buscamos la temporal
        rlTempVATAMOUNT.FindSet();
        //bucle for 
        for i := 1 to 4 do begin
            xTotalIVA[1, i] := Format(rlTempVATAMOUNT."VAT Base");

            xTotalIVA[3, i] := Format(rlTempVATAMOUNT."VAT Amount");
            xTotalIVA[4, i] := Format(rlTempVATAMOUNT."Amount Including VAT");
            if rlTempVATAMOUNT.Next() = 0 then begin
                i := 1000;
            end;
        end;

    end;

    //variables 
    var
        rCompanyInfo: Record "Company Information";
        xSalesHeader: Record "Sales Header";
        cuFormatAddress: Codeunit "Format Address";
        xCopias: Integer;
        rXcolor: Text;
        xCompanyAddr: array[8] of Text;
        xCustAddr: array[8] of Text;
        xDocumentLaber: Text;
        //avariabele de tipi texto , variable no te hace falta
        xDocumentNo: Text;
        xTotalIVA: array[1, 4] of Text;

}