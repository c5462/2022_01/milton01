/// <summary>
/// PageExtension MXCCompaniesExtC01 (ID 50106) extends Record Companies.
/// </summary>
pageextension 50006 "MXCCompaniesExtC01" extends Companies
{
    layout
    {
        addlast(Control1)
        {
            field(MXCClientesC01; MXCClientesF(false))
            {
                Caption = 'Nº Clientes';
                ApplicationArea = All;
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    MXCClientesF(true);
                end;
            }
            field(MXCProveedoresC01; MXCProveedoresF(false))
            {
                Caption = 'Nº Proveedores';
                ApplicationArea = All;
                DrillDown = true;
                trigger OnDrillDown()
                begin

                end;
            }
            field(MXCCuentaC01; MXCuentaF(false))
            {
                Caption = 'Nº Cuenta';
                ApplicationArea = All;
                DrillDown = true;
                trigger OnDrillDown()
                begin

                end;
            }

            field(MXCProductoC01; MXCProductos(false))
            {
                Caption = 'Nº Productos';
                ApplicationArea = All;
                DrillDown = true;
                trigger OnDrillDown()
                begin

                end;
            }
        }
    }

    actions
    {
        addlast(processing)
        {
            action(MXCRestaurarCopiaC01)
            {
                Caption = 'Restaurar copia seguridad';
                ApplicationArea = all;

                Image = ImportDatabase;
                trigger OnAction()
                begin
                    RestaurarCopiaSeguridadF();
                end;
            }
            action(MXCBACKUPC01)
            {
                ApplicationArea = all;
                Caption = 'Copia de Seguridad';
                trigger OnAction()
                begin
                    CopiaSeguridadF();
                end;

            }

        }
    }




    local procedure MXCClientesF(pDrillFDowm: Boolean):
    Integer
    var
        rlCustomer: Record Customer;
    begin
        DatosTablaF(rlCustomer, pDrillFDowm);
    end;

    local procedure MXCProveedoresF(pDrillFDowm: Boolean): Integer
    var
        rlVendor: Record Vendor;
    begin
        exit(DatosTablaF(rlVendor, pDrillFDowm));
    end;

    local procedure MXCuentaF(pDrillFDowm: Boolean): Integer
    var
        rlGLAccount: Record "G/L Account";
    begin
        exit(DatosTablaF(rlGLAccount, pDrillFDowm));
    end;

    local procedure MXCProductos(pDrillFDowm: Boolean): Integer
    var
        rlitem: Record item;
    begin
        exit(DatosTablaF(rlitem, pDrillFDowm));
    end;


    local procedure EjemploRecordRefF()
    var
        xlRecRef: RecordRef;
        i: Integer;
    begin
        xlRecRef.Open(Database::Item);
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    xlRecRef.FieldIndex(i).Value;
                end;
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());
                xlRecRef.Field(4).Validate('');//poner el campo  vacip
                xlRecRef.Modify(true)
            until xlRecRef.Next() = 0;
        end;
        //xlRecRef.KeyIndex(1).FieldCount contar las claves de cada tabla 
    end;

    local procedure DatosTablaF(pRecVariant: Variant; pDrillFDowm: Boolean): Integer
    var
        xlRecRef: RecordRef;
        rlcompany: Record Company;
    begin
        //si  pRecVariant es un record mételo en mi RecordRef
        if pRecVariant.IsRecord then begin


            xlRecRef.GetTable(pRecVariant);//pasa la tabla que pasa el recordRef
            if rlcompany.get(Rec.Name) then begin

                //// nombre de empresa distinta
                if Rec.Name <> CompanyName then begin
                    xlRecRef.ChangeCompany(Rec.Name);
                end;
                if pDrillFDowm then begin
                    pRecVariant := xlRecRef;
                    Page.Run(0, pRecVariant);// a una pagina no podemos enviar un recoref    
                end else begin
                    exit(xlRecRef.Count);
                end;
            end;
        end;
    end;

    local procedure NumeroTablaF(pNombreTabla: Text): Integer
    var
        rlAllObjWithCaption: Record AllObjWithCaption;
    begin
        rlAllObjWithCaption.SetRange("Object Name", pNombreTabla);
        rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
        //aplicados los filtros 
        //se va traer el primero de los 
        rlAllObjWithCaption.SetLoadFields("Object ID");
        if rlAllObjWithCaption.FindFirst() then begin
            //que registros necesitamos 



            exit(rlAllObjWithCaption."Object ID");
        end;

    end;

    local procedure CopiaSeguridadF()
    var
        rlAllObjWithCaption: Record AllObjWithCaption;
        rlCompany: Record Company;
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary;
        culTypeHelper: Codeunit "Type Helper";
        xlRecRef: RecordRef;
        XlInStr: Instream;
        i: Integer;
        xlOutSteam: OutStream;
        xlNombreFichero: Text;
        xlTextBuider: TextBuilder;
    begin
        CurrPage.SetSelectionFilter(rlCompany);//para poder filtras por las empresas que queramos 
        //1 bucle de tabals 
        if rlCompany.FindSet(false) then begin
            repeat
                //escribir en el fichero
                //inicio de empresa
                xlTextBuider.Append('IE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
                //filtramos los datos 
                //2. bucle de tablas 
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '%1|%2|%3|%4', Database::"G/L Account", Database::Customer, Database::Vendor, Database::Item);
                //

                if rlAllObjWithCaption.FindSet(false) then begin
                    //2 bucle de tablas
                    repeat
                        xlTextBuider.Append('IT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                        //3 bucle de registros
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);
                        //xlRecRef.ChangeCompany(rlCompany.Name); o 
                        if xlRecRef.FindSet(false) then begin
                            repeat
                                //---------------------------
                                //inicio de registros
                                xlTextBuider.Append('IR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                                //Bucle de campos
                                for i := 1 to xlRecRef.FieldCount do begin
                                    //xlTextBuider.Append('<'+xlRecRef.FieldIndex(i).Name +'>'+ '·' + '<'+Format(xlRecRef.FieldIndex(i).Value)+'>');
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin

                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuider.Append(StrSubstNo('<%1>·<%2>', xlRecRef.FieldIndex(i).Name, xlRecRef.FieldIndex(i).Value) + culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                end;

                                //Fin de registros
                                xlTextBuider.Append('FR<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        //cerramos registro
                        xlRecRef.Close();
                        xlTextBuider.Append('FT<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                    until rlAllObjWithCaption.Next() = 0;
                end;
                //Fin de empresa

                xlTextBuider.Append('FE<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());

            //escribir en un contenedor y descargar el fichero

            //creamos el contenedor 
            until rlCompany.Next() = 0;
            TempLMXCConfiguracionC01.MiBlob.CreateOutStream(xlOutSteam, TextEncoding::UTF8);
            //escribir 
            xlOutSteam.WriteText(xlTextBuider.ToText());
            TempLMXCConfiguracionC01.MiBlob.CreateInStream(XlInStr);
            xlNombreFichero := 'CopiaSeguridad.bck';
            if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
                Error('El fichero no se ha podido descargar');
            end;
        end;
    end;



    //funcion que ccn el nombre del campo y el numero de la tabla me devuleva el numero de la tabla
    local procedure NumCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record Field;
    begin
        //numero de la tabla, para 
        rlField.SetRange(TableNo, pNumTabla);
        rlField.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");
        //filtrmos de esta tabla 
        //solo el primero 
        if rlField.FindFirst() then begin
            exit(rlField."No.");
        end;
    end;

    local procedure CampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        TempLCustomer: Record Customer temporary;
        TempLConfiguracionC01: Record MXCConfiguracionC01 temporary;
        TempLItem: Record Item temporary;
        xlBigInteger: BigInteger;
        xlbool: Boolean;
        xlDate: Date;
        xlDateTime: DateTime;
        xlDecimal: Decimal;
        xlGuid: Guid;
        i: Integer;
        xlTime: Time;
    begin
        ///trabajo de campos
        /// 
        case pFieldRef.Type of

            // pFieldRef.Type::Code, pFieldRef.Type::Text:
            //     exit(pValor);
            pFieldRef.Type::BigInteger, pFieldRef.Type::Integer:
                if Evaluate(xlBigInteger, pValor) then begin
                    exit(xlBigInteger);
                end;
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToUpper() = pValor.ToUpper() then begin
                            exit(i);
                        end;
                    end;
                end;
            pFieldRef.Type::Boolean:
                begin
                    if Evaluate(xlbool, pValor) then begin
                        exit(xlbool)
                    end;
                end;
            pFieldRef.Type::Blob:

                exit(TempLConfiguracionC01.MiBlob);

            pFieldRef.Type::Decimal:
                begin

                    if Evaluate(xlDecimal, pValor) then begin
                        exit(xlDecimal);
                    end;
                end;
            pFieldRef.Type::Date:
                begin

                    if Evaluate(xlDate, pValor) then begin
                        exit(xlDate);
                    end
                end;

            pFieldRef.Type::DateTime:
                begin
                    if Evaluate(xlDateTime, pValor) then begin
                        exit(xlDateTime);
                    end
                end;
            pFieldRef.Type::Guid:
                if Evaluate(xlGuid, pValor) then begin
                    exit(xlGuid);
                end;
            pFieldRef.Type::Media:
                exit(TempLCustomer.Image);
            pFieldRef.Type::MediaSet:
                exit(TempLItem.Picture);
            pFieldRef.Type::Time:
                if Evaluate(xlTime, pValor) then begin
                    exit(xlTime);
                end;
            else
                exit(pValor);
        end;
    end;

    local procedure RestaurarCopiaSeguridadF()
    var
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary;
        xlInstr: Instream;
        xlinea: Text;
        xlRecRef: RecordRef;
        separadorCampoValor: Label '·';
    begin
        //1) Subir fihcero de copias de seuridad,por lo que necesitamos un blob, es nuestro contenedor 
        TempLMXCConfiguracionC01.MiBlob.CreateInStream(xlInstr, TextEncoding::UTF8);
        if UploadIntoStream('', xlInstr) then begin
            //2)Recorremos el fichero
            while not xlInstr.EOS do begin
                xlInstr.ReadText(xlinea); //va leyendo linea a linea ,mientras no sea final de fichero ba leyendo linea linea
                //Message(xlinea);
                case true of
                    //3) si es inico de tabla .abrir el resgistro 
                    //xlinea[1] = 'IT':
                    copystr(xlinea, 1, 3) = 'IT<':
                        begin
                            if xlRecRef.Number <> 0 then begin
                                xlRecRef.Close();
                            end;
                            xlRecRef.Open(NumeroTablaF(CopyStr(xlinea, 4).TrimEnd('>')));

                        end;
                    copystr(xlinea, 1, 3) = 'IR<':
                        begin
                            //4) si es inicio de registro,inicialixamos resgiastro
                            xlRecRef.Init();
                        end;
                    //xlinea[1]='<':
                    xlinea.StartsWith('<'):
                        begin
                            //5) por cada campo, asigar su valor
                            //funcion que ccon el nombre del campo y el numero de la tabla me debvuleva el numero de la tabla
                            // NumCampoF(xlRecRef.Number, xlinea.Split('·').Get(1).TrimStart('<').TrimEnd('>'));
                            //
                            if xlRecRef.Field(NumCampoF(xlRecRef.Number, xlinea.Split('·').Get(1).TrimStart('<').TrimEnd('>'))).Class = FieldClass::Normal then begin
                                xlRecRef.Field(NumCampoF(xlRecRef.Number, xlinea.Split('·').Get(1).TrimStart('<').TrimEnd('>'))).Value := CampoYValorF(xlRecRef.Field(NumCampoF(xlRecRef.Number, xlinea.Split('·').Get(1).TrimStart('<').TrimEnd('>'))), xlinea.Split(separadorCampoValor).Get(2).TrimStart('<').TrimEnd('>'));
                                //xlinea.Split(separadorCampoValor).Get(2).TrimStart('<').TrimEnd('>');
                                //no podemos importar campos calculados , si el campo es 
                                //si el campo donde voy asignar el valor 
                                //seria hacer un case para evaluar, los valores 
                                //funcion 
                            end;
                        end;
                    copystr(xlinea, 1, 3) = 'FR<':
                        //6) Si es fin de regisgtro, modificar registro, insertar registro
                        begin
                            xlRecRef.Insert(false);
                        end;

                    copystr(xlinea, 1, 3) = 'FT<':
                        //7) so es fin de tabla, cerrar el registro

                        xlRecRef.Close();


                end;
            end;
            if xlRecRef.Number <> 0 then begin
                xlRecRef.Close();
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor. Erorr (%1)', GetLastErrorText());
        end;
    end;

}