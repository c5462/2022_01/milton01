/// <summary>
/// PageExtension MXCPurchase InvoiceExtC01 (ID 80108) extends Record Purchase Invoice.
/// </summary>
pageextension 50008 "MXCPurchase InvoiceExtC01" extends "Purchase Invoice"
{
    layout
    {
        modify("No.")
        {
            Visible = true;
        }
    }
}
