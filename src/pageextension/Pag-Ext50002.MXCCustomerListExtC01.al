/// <summary>
/// pagina extendida
/// </summary>
pageextension 50002 "MXCCustomerListExtC01" extends "Customer List"
{

    actions
    {
        addfirst(General)
        {
            action(MXCtestC01)
            {
                ApplicationArea = All;
                Image = NewCustomer;
                Caption = 'test';
                trigger OnAction()
                var
                    Customer: Record Customer;
                begin
                    Customer.get('10000');
                    ModifyTest(Customer);
                end;
            }
            action(MXCVariablesGlobalesAppC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba Variables globales a BC';
                trigger OnAction()
                var
                    // cuMXCFuncionesAppC01: Codeunit MXCFuncionesAppC01;
                    newLabel: Label 'El valor actual es %1, lo cambiamos al valor %2';

                begin
                    Message(newLabel, cuMXCFuncionesAppC01.GetSwitchF(), not cuMXCFuncionesAppC01.GetSwitchF());
                    cuMXCFuncionesAppC01.SetSwitchF(not cuMXCFuncionesAppC01.GetSwitchF());
                    //cuando pongo un tanto porcuenro es un parametro
                end;
            }
            action(MXCPruebaCapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba CodeUni de captura de errores';

                trigger OnAction()
                var
                    culCapturaErroresC01: Codeunit MXCFuncionesErroresC01;
                begin
                    //EJECUCION EN FORMA MODAL PORQUE NECESITA QUE INTERACTUE CON EL USUARIO
                    if culCapturaErroresC01.Run() then begin
                        Message('El botón dice que se a ejecutado correctamente');
                    end else begin
                        Message('No se ha podido ejecutar la codeUnit. Motivo : (%1)', GetLastErrorText());
                    end;
                end;
            }
            action(MXCPrueba2CapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba de la Funcion de  captura de errores';

                trigger OnAction()
                //variables
                var
                    culCapturaErroresC01: Codeunit MXCFuncionesErroresC01;
                    xlCodProveedor: Code[20];
                    i: Integer;
                begin
                    xlCodProveedor := '10000';
                    for i := 1 to 5 do begin
                        //EJECUCION EN FORMA MODAL PORQUE NECESITA QUE INTERACTUE CON EL USUARIO
                        if culCapturaErroresC01.FuciontoCaptureErrorF(xlCodProveedor) then begin
                            Message('Se ha creado el provedor %1', xlCodProveedor);
                        end;
                        //  else begin
                        //     Message('No se ha podido ejecuytar la Funcion . Motivo : (%1)', GetLastErrorText());
                        // end;
                        xlCodProveedor := IncStr(xlCodProveedor);
                        //como funcinan inStr manttiene la longuitud

                    end;
                end;
            }
            action(MXCMXExpCteC01)
            {
                Caption = 'Exportar Ctes exel';
                ApplicationArea = all;
                Image = Excel;
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    i: Integer;
                begin
                    //crer un nuevo libro nuevo en exel
                    TempLExcelBuffer.CreateNewBook('Clientes');
                    //lo preparas para rellenar las hojas exel 
                    //rellenar las cabeceras

                    AsignaCeldaF(1, 1, Rec.FieldCaption("No."), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 2, Rec.FieldCaption(Name), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 3, Rec.FieldCaption("Responsibility Center"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 4, Rec.FieldCaption("Location Code"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 5, Rec.FieldCaption("Phone No."), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 6, Rec.FieldCaption(Contact), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 7, Rec.FieldCaption("Balance (LCY)"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 8, Rec.FieldCaption("Balance Due (LCY)"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 9, Rec.FieldCaption("Sales (LCY)"), true, true, TempLExcelBuffer);
                    AsignaCeldaF(1, 10, Rec.FieldCaption("Payments (LCY)"), true, true, TempLExcelBuffer);
                    CurrPage.SetSelectionFilter(rlCustomer);
                    if rlCustomer.FindSet(false) then begin
                        //recorremos todos los registos
                        i := 1;
                        repeat
                            i += 1;
                            AsignaCeldaF(i, 1, rlCustomer."No.", false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 2, rlCustomer.Name, false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 3, rlCustomer."Responsibility Center", false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 4, rlCustomer."Location Code", false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 5, rlCustomer."Phone No.", false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 6, rlCustomer.Contact, false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 7, Format(rlCustomer."Balance (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 8, Format(rlCustomer."Balance Due (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 9, Format(rlCustomer."Sales (LCY)"), false, false, TempLExcelBuffer);
                            AsignaCeldaF(i, 10, Format(rlCustomer."Payments (LCY)"), false, false, TempLExcelBuffer);


                        until rlCustomer.Next() = 0;
                    end;

                    TempLExcelBuffer.WriteSheet('', '', '');
                    TempLExcelBuffer.CloseBook();
                    //nos abre el exel
                    TempLExcelBuffer.OpenExcel();
                end;
            }

            action(MXCImportarC01)
            {
                ApplicationArea = all;
                Caption = 'Importar excel';
                Image = Import;
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    xlInstr: Instream;
                    xlFicheroCte: Text;
                    xlHoja: Text;
                begin
                    if UploadIntoStream('Selecione el fichero exel para importar', '', 'Ficheros Excel (*.xls)|*.xls;*.xlsx', xlFicheroCte, xlInstr) then begin
                        xlHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInstr);
                        if xlHoja = '' then begin
                            Error('Error cancelado ');
                        end;
                        TempLExcelBuffer.OpenBookStream(xlInstr, xlHoja);
                        TempLExcelBuffer.ReadSheet();
                        TempLExcelBuffer.SetFilter("Row No.", '>%1', 1);
                        if TempLExcelBuffer.FindSet(false) then begin
                            repeat
                                case TempLExcelBuffer."Column No." of
                                    1:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    2:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate(Name, TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    3:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("Responsibility Center", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    4:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("Location Code", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    5:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate("Phone No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    6:
                                        begin
                                            rlCustomer.Init();
                                            rlCustomer.Validate(Contact, TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    7:
                                        begin
                                            rlCustomer.Modify();
                                        end;
                                    8:
                                        begin
                                            rlCustomer.Modify();
                                        end;
                                    9:
                                        begin
                                            rlCustomer.Modify();
                                        end;
                                    10:
                                        begin
                                            rlCustomer.Modify();
                                        end;

                                end

                            until TempLExcelBuffer.Next() = 0;
                        end;
                        //  Page.Run(Page::MXCExelBufferC01, TempLExcelBuffer);
                    end else begin
                        Error('No se ha podido cargar el fihcero');
                    end;
                end;


            }
            action(MXCPaginaFiltroC01)
            {
                ApplicationArea = All;
                Caption = 'Pagina filtros';
                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    xlPaginaFlitros: FilterPageBuilder;
                    clClientes: Label 'Clientes';

                begin
                    xlPaginaFlitros.PageCaption('Selec los clientes a procesar');
                    xlPaginaFlitros.AddRecord(clClientes, rlCustomer);
                    xlPaginaFlitros.AddField(clClientes, rlCustomer."No.");
                    xlPaginaFlitros.AddField(clClientes, rlCustomer.MXCFechaUltimaVacunaC01);
                    // xlPaginaFlitros.AddField(clMovCte)
                    // xlPaginaFlitros.AddRecord(clClientes, rlCustomer);

                    xlPaginaFlitros.AddRecord(clClientes, rlCustomer);

                end;
            }





        }

        addlast(History)
        {
            action(MXCFacturasAbonoC01)
            {
                Caption = 'Facturas/Abono';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    facturasyAbonosF();
                end;
            }
            action(MXCExportarC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar';
                trigger OnAction()
                begin
                    ExportaCtesF();
                end;
            }
            action(MXCSumatorioC01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 01';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTinicio: DateTime;
                    xTotal: Decimal;
                begin
                    xlTinicio := CurrentDateTime;
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    if rlDetailedCustLedgEntry.FindSet(false) then begin
                        repeat
                            xTotal += rlDetailedCustLedgEntry.Amount;
                        until rlDetailedCustLedgEntry.Next() = 0;
                    end;
                    Message('Total = %1. HA  %2 ', xTotal, CurrentDateTime - xlTinicio);


                end;
            }
            action(MXCSumatorio02C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 02';
                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTinicio: DateTime;
                    xlTotal: Decimal;
                begin
                    xlTinicio := CurrentDateTime;
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No.");
                    // if rlDetailedCustLedgEntry.FindSet(false) then begin
                    //     repeat
                    //         xTotal += rlDetailedCustLedgEntry.Amount;
                    //     until rlDetailedCustLedgEntry.Next() = 0;
                    // end;
                    rlDetailedCustLedgEntry.CalcSums(Amount);
                    xlTotal := rlDetailedCustLedgEntry.Amount;
                    Message('Total = %1. HA TARDADO %2 ', xlTotal, CurrentDateTime - xlTinicio);
                end;
            }
            action(MXCSumatorio03C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 03';
                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlTinicio: DateTime;
                    xTotal: Decimal;
                begin
                    xlTinicio := CurrentDateTime;

                    rlCustLedgerEntry.SetRange("Customer No.", Rec."No.");
                    //se gana mucho en rendimiento si no se hace modificaciones en la bbdd ,solo vamos a leer sin mod
                    rlCustLedgerEntry.SetLoadFields(Amount);
                    //hacelo mismo  se pone despues del fin set , 
                    rlCustLedgerEntry.SetAutoCalcFields(Amount);
                    // rlCustLedgerEntry.CalcFields(Amount);//a este lo cambiamos para ganar en rendimiento 
                    if rlCustLedgerEntry.FindSet(false) then begin
                        repeat
                            //registro del campo calculadfo 
                            xTotal += rlCustLedgerEntry.Amount;
                        until rlCustLedgerEntry.Next() = 0;
                    end;
                    Message('Total = %1. HA TARDADO (%2) ', xTotal, CurrentDateTime - xlTinicio);
                end;
            }
            action(MXCSumatorio04C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio 04';
                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlTinicio: DateTime;
                    xTotal: Decimal;
                begin
                    xlTinicio := CurrentDateTime;
                    rlCustLedgerEntry.SetRange("Customer No.", Rec."No.");
                    xTotal := rlCustLedgerEntry.Amount;
                    Message('Total = %1. HA TARDADO %2', xTotal, CurrentDateTime - xlTinicio);
                end;


            }
            action(MXCCopiarEnProveedoresC01)
            {
                Caption = 'Clonar en Proveedores';
                Image = Addresses;
                ApplicationArea = all;
                //si queremos que exista la posiblilidad de ponerlo en los 3 puntitos
                Promoted = true;
                Scope = Repeater;
                trigger OnAction()
                var
                    rlVendor: Record Vendor;

                begin
                    if rlVendor.Get(Rec."No.") then begin
                        Error('El Registro: %1 YA EXISTE en Proveedores', Rec."No.");
                    end else begin
                        rlVendor.Init();
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert(true);
                        Message('Cliente creado correctamente');
                    end;
                end;
            }

        }

    }

    //var Global
    var
        cuMXCFuncionesAppC01: Codeunit "MXCVariablesGlobalesAppC01";


    //
    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('Desea salir de esta pagina', false))
    end;

    local procedure facturasyAbonosF()
    var
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
        //  pglPostedSalesInvoice: Page "Posted Sales Invoices";
        TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary;


    begin
        //Recorremos las facturas del cliente y las insertamos en la tabla temporal
        //hacemos el filtro hecho
        TempLSalesInvoiceHeader.SetRange("Bill-to Customer No.", Rec."No.");
        //pglMXCListaPLanVacunacionC01.SetRecord(rlMXCPllanVacunacionC01);
        // if rlSalesInvoiceHeader.ReadPermission, no 
        // if rlSalesInvoiceHeader.WritePermission

        //cuando tienen los filtros aplicados.
        //Message(rlSalesInvoiceHeader.GetFilter("Bill-to Customer No."));


        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.Copy(rlSalesInvoiceHeader);

                // TempLSalesInvoiceHeader.Validate("No.", Rec."Bill-to Customer No.");
                //Rec.Validate("No.", 'F' + TempLSalesInvoiceHeader."No.");
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesInvoiceHeader.Next() = 0;
        end;
        TempLSalesInvoiceHeader.SetRange("Bill-to Customer No.", Rec."No.");
        Message(rlSalesCrMemoHeader.GetFilters);

        //   ""     los abonos del cliente , y los insertamo en la tabla temporal
        if rlSalesCrMemoHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader);
                TempLSalesInvoiceHeader.Insert(false);
            until rlSalesCrMemoHeader.Next() = 0;
        end;
        //mostraremos la pagina con fac y abonos
        Page.Run(0, TempLSalesInvoiceHeader);
        // pglPostedSalesInvoice.SetRecord(TempLSalesInvoiceHeader);
        // pglPostedSalesInvoice.SetTableView(TempLSalesInvoiceHeader);
        // pglPostedSalesInvoice.Run();




    end;

    local procedure ExportaCtesF()
    var
        rlCustomer: Record Customer;
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary;
        xlInStream: Instream;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4', Locked = true;
        clSeparador: Label '·', Locked = true;
        xlOutStream: OutStream;
        xlNombreFichero: Text;
    begin
        TempLMXCConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::UTF8);
        rlCustomer := Rec;//rCustomr conecta con la pagina, es reutilizable (Te trae los registros)
        //rlCustomer.get(Rec."No.");//la variable se rellena de forma directa se trae el cliente ,cuyo codigo es este 
        rlCustomer.Find();//como ya tiene los valores de la clave principal asignados, ya esta conectada con SqL
        //rlCustomer.CalcSums()
        rlCustomer.CalcFields("Balance (LCY)");
        xlOutStream.WriteText(StrSubstNo(clCadenaCabecera, clSeparador, rlCustomer."No.", rlCustomer.Name, rlCustomer.CalcFields("Balance (LCY)")));
        // Escribir el registro
        //Rec.CalcFields("Balance (LCY)");
        xlOutStream.WriteText();
        TempLMXCConfiguracionC01.MiBlob.CreateInStream(xlInStream);
        xlNombreFichero := 'Clientes.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('Problema al descargar fichero');
        end;
    end;

    local procedure AsignaCeldaF(pFila: Integer; pColumna: Integer; pContenido: Text; pNegrita: Boolean; pItalica: Boolean; var TempLExcelBuffer: Record "Excel Buffer" temporary)

    begin
        TempLExcelBuffer.Init();
        TempLExcelBuffer.Validate("Row No.", pFila);
        TempLExcelBuffer.Validate("Column No.", pColumna);
        TempLExcelBuffer.Insert(true);
        TempLExcelBuffer.Validate("Cell Value as Text", pContenido);
        TempLExcelBuffer.Validate(Bold, pNegrita);
        TempLExcelBuffer.Validate(Italic, pItalica);
        TempLExcelBuffer.Modify(true);
    end;

    local procedure ModifyTest(pVariant: Variant)
    var
        TempCustomer: Record Customer temporary;
        RecordRef: RecordRef;
    begin
        if Confirm('con gettable', true) then
            RecordRef.GetTable(pVariant)
        else
            RecordRef.Open(pVariant);
        RecordRef.Field(TempCustomer.FieldNo("Address 2")).Validate(RecordRef.Field(TempCustomer.FieldNo(Address)).Value);
        RecordRef.Modify(true);
    end;
}
