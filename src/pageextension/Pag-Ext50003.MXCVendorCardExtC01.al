/// <summary>
/// PageExtension MXCVendorCardExtC01 (ID 80103) extends Record Vendor Card.
/// </summary>
pageextension 50003 "MXCVendorCardExtC01" extends "Vendor Card"
{

    actions
    {
        addlast(processing)
        {
            action(MXCCopiarEnClientesC01)
            {
                Caption = 'Clonar en Clientes';
                Image = Addresses;
                ApplicationArea = all;
                //si queremos que exista la posiblilidad de ponerlo en los 3 puntitos
                Promoted = true;
                Scope = Repeater;
                trigger OnAction()
                var
                    rlCustomer: Record Customer;

                begin
                    /*
                                        //devuelve el nombre del cliente
                                        Message('%1', rlCustomer.Name);

                                        //devuelve el numero del provedor
                                        Message('%1', Rec."No.");

                                        //existe este objeto==Si
                                        Message('%1', Rec.get('01254796'));

                                        //existe el objeto vacio ==no
                                        Message('%1', Rec.get(rlCustomer.Name));
                                        //devuelve el No. de ccliete
                                        //tendria que devolver un sii==> porque devuelve el el cliente en caso de existir
                                        Message('%1', rlCustomer.get(Rec."No."));
                                        */
                    if rlCustomer.Get(Rec."No.") then begin
                        Error('El Registro: %1 YA EXISTE en Clientes', Rec."No.");
                    end else begin
                        rlCustomer.Init();
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.Insert(true);
                        Message('Cliente creado correctamente');
                    end;
                end;
            }

        }
    }

}


