/// <summary>
/// PageExtension MXCPurchaseOrderExtC01 (ID 80107) extends Record Purchase Order.
/// </summary>
pageextension 50007 "MXCPurchaseOrderExtC01" extends "Purchase Order"
{
    actions
    {
        addlast(processing)
        {
            //MXC- I prueba de tarea "Actualizar la fecha de recepcion esperada"
            action(MXCActualizarFchExpectedC01)
            {
                Caption = 'Actualizar Fecha Recepción esperada.';
                ApplicationArea = All;
                Image = Export;
                trigger OnAction()
                begin
                    ActualizarFechaEntrPlanificadaF(Rec);
                end;
            }
            action(MXCExportarC01)
            {
                Caption = 'Exportar lineas pedido';
                ApplicationArea = All;
                Image = Export;
                trigger OnAction()
                begin
                    ExportarLineasPedidoF(Rec);
                end;
            }
            //MXC-F
        }
    }

    local procedure ActualizarFechaEntrPlanificadaF(pRec: Record "Purchase Header")
    var
        TempLExcelBuffer: Record "Excel Buffer" temporary;
        "Purchase Line": Record "Purchase Line";
        xlInstr: Instream;
        cExcelTitulo: Label 'Selecione el fichero exel para importar';
        xlFicheroCte: Text;
        xlHoja: Text;
        xlNumDoc: Text;
        xlNum: Text;
        xlCant: Decimal;
        xlfecha: Date;
    begin
        if UploadIntoStream(cExcelTitulo, '', 'Ficheros Excel (*.xls)|*.xls;*.xlsx', xlFicheroCte, xlInstr) then begin
            xlHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInstr);
            if xlHoja = '' then begin
                Error('Error cancelado ');
            end;
            if pRec.Status = pRec.Status::Open then begin
                TempLExcelBuffer.OpenBookStream(xlInstr, xlHoja);
                TempLExcelBuffer.ReadSheet();
                TempLExcelBuffer.SetFilter("Row No.", '>1');
                if TempLExcelBuffer.FindSet(false) then begin
                    repeat
                        case TempLExcelBuffer."Column No." of
                            1:
                                begin
                                    xlNumDoc := TempLExcelBuffer."Cell Value as Text";
                                end;
                            2:
                                begin
                                    xlNum := TempLExcelBuffer."Cell Value as Text";
                                end;
                            3:
                                begin
                                    Evaluate(xlCant, TempLExcelBuffer."Cell Value as Text");
                                end;
                            4:
                                begin
                                    Evaluate(xlfecha, TempLExcelBuffer."Cell Value as Text");
                                end;
                            5:
                                begin
                                    "Purchase Line".SetRange("Document Type", pRec."Document Type");
                                    "Purchase Line".SetRange("Document No.", xlNumDoc);
                                    "Purchase Line".SetRange(Type, "Purchase Line".Type::Item);
                                    "Purchase Line".SetRange("No.", xlNum);
                                    "Purchase Line".SetRange(Quantity, xlCant);
                                    "Purchase Line".SetRange("Expected Receipt Date", xlfecha);
                                    if "Purchase Line".FindSet(true, true) then begin
                                        Evaluate("Purchase Line"."Expected Receipt Date", TempLExcelBuffer."Cell Value as Text");
                                        "Purchase Line".Modify(true);
                                    end;
                                end;
                        end
                    until TempLExcelBuffer.Next() = 0;
                end;

            end else begin
                Error('Este pedido no esta abierto');
            end;
            ;
        end else begin
            Error('No se ha podido cargar el fichero');
        end;
    end;

    local procedure ExportarLineasPedidoF(pRec: Record "Purchase Header")
    var
        TempLExcelBuffer: Record "Excel Buffer" temporary;
        rlPurchaseLine: Record "Purchase Line";
        cNewFchPlanf: Label '·Nueva Fecha entrega planificada·';
        i: Integer;
    begin
        TempLExcelBuffer.CreateNewBook('Pedidos venta');
        AsignaCeldaF(1, 1, rlPurchaseLine.FieldCaption("Document No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 2, rlPurchaseLine.FieldCaption("No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 3, rlPurchaseLine.FieldCaption(Quantity), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 4, rlPurchaseLine.FieldCaption("Expected Receipt Date"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 5, cNewFchPlanf, true, true, TempLExcelBuffer);
        rlPurchaseLine.SetRange("Document No.", pRec."No.");
        if rlPurchaseLine.FindSet(false) then begin
            i := 1;
            repeat
                i += 1;
                AsignaCeldaF(i, 1, rlPurchaseLine."Document No.", false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 2, rlPurchaseLine."No.", false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 3, Format(rlPurchaseLine.Quantity), false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 4, Format(rlPurchaseLine."Expected Receipt Date"), false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 5, Format(rlPurchaseLine."Expected Receipt Date"), false, false, TempLExcelBuffer);
            until rlPurchaseLine.Next() = 0;
        end;
        TempLExcelBuffer.WriteSheet('', '', '');
        TempLExcelBuffer.CloseBook();
        TempLExcelBuffer.OpenExcel();
    end;

    local procedure AsignaCeldaF(pFila: Integer; pColumna: Integer; pContenido: Text; pNegrita: Boolean; pItalica: Boolean; var TempLExcelBuffer: Record "Excel Buffer" temporary)

    begin
        TempLExcelBuffer.Init();
        TempLExcelBuffer.Validate("Row No.", pFila);
        TempLExcelBuffer.Validate("Column No.", pColumna);
        TempLExcelBuffer.Insert(true);
        TempLExcelBuffer.Validate("Cell Value as Text", pContenido);
        TempLExcelBuffer.Validate(Bold, pNegrita);
        TempLExcelBuffer.Validate(Italic, pItalica);
        TempLExcelBuffer.Modify(true);
    end;

}

