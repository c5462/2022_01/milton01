/// <summary>
/// PageExtension MXCSalesOrderExtC01 (ID 80104) extends Record Sales Order.
/// </summary>
pageextension 50004 "MXCSalesOrderExtC01" extends "Sales Order"
{
    actions
    {
        addlast(processing)
        {
            action(MXCExportarPedidosVentaC01)
            {
                Caption = 'Exportar Lsita de Pedido Venta';
                ApplicationArea = All;
                Image = ExplodeRouting;
                trigger OnAction()
                begin
                    ExportarPedidosVentaF();
                    Message('vamos a exportalo bien');
                end;
            }



        }

        addlast(processing)
        {
            //MXC- I prueba de tarea "Actualizar la fecha de recepcion esperada"
            action(MXCActualizarFchExpectedC01)
            {
                Caption = 'Actualizar Fecha Recepción esperada.';
                ApplicationArea = All;
                Image = Export;
                trigger OnAction()
                begin
                    ActualizarFechaEntrPlanificadaF(Rec);
                end;
            }

            action(MXCExportarC01)
            {
                Caption = 'Exportar lineas pedido';
                ApplicationArea = All;
                Image = Export;
                trigger OnAction()
                begin
                    ExportarLineasPedidoF();
                end;
            }
            //MXC-F

        }


    }

    local procedure ExportarPedidosVentaF()
    var
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        //campos 
        XlInStr: Instream;
        //cabecera
        ClCadenaCabecera: Label 'C%1%2%%1', Locked = true;
        ClCadenaLinea: Label 'L%1%2%%1', Locked = true;
        ClSeparador: Label '.', Locked = true;
        ClSeparadorLinea: Label '.', Locked = true;
        xlOutStr: OutStream;
        xlNombreFichero: Text;
    begin
        TempLMXCConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::Windows);
        Message('Seleecciona los pedidos a exportar');

        CurrPage.SetSelectionFilter(rlSalesHeader);
        //recorremos 
        if rlSalesHeader.FindSet(false) then begin
            //Message('%1', rlSalesHeader);
            repeat
                xlOutStr.WriteText(StrSubstNo(ClCadenaCabecera, ClSeparador, rlSalesHeader."No.", rlSalesHeader."Document Date"));
                xlOutStr.WriteText();
            until rlSalesHeader.Next() = 0;
            rlSalesLine.SetRange("Document No.", rlSalesHeader."No.");
            if rlSalesLine.FindSet(false) then begin
                repeat
                    xlOutStr.WriteText(StrSubstNo(ClCadenaLinea, ClSeparadorLinea, rlSalesLine."Document No."));
                    xlOutStr.WriteText();
                until rlSalesLine.Next() = 0;
            end;

        end;
        TempLMXCConfiguracionC01.MiBlob.CreateInStream(XlInStr);
        //xlNombreFichero := 'Cabecera.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;

    end;

    local procedure ActualizarFechaEntrPlanificadaF(pRec: Record "Sales Header")
    var
        TempLExcelBuffer: Record "Excel Buffer" temporary;
        rlSalesLine: Record "Sales Line";
        xlInstr: Instream;
        cExcelTitulo: Label 'Selecione el fichero exel para importar';
        xlFicheroCte: Text;
        xlHoja: Text;
        xlNumDoc: Text;
        xlNum: Text;
        xlCant: Decimal;
        xlfecha: Date;
    begin
        if UploadIntoStream(cExcelTitulo, '', 'Ficheros Excel (*.xls)|*.xls;*.xlsx', xlFicheroCte, xlInstr) then begin
            xlHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInstr);
            if xlHoja = '' then begin
                Error('Error cancelado ');
            end;
            TempLExcelBuffer.OpenBookStream(xlInstr, xlHoja);
            TempLExcelBuffer.ReadSheet();
            TempLExcelBuffer.SetFilter("Row No.", '>1');
            if TempLExcelBuffer.FindSet(false) then begin
                repeat
                    case TempLExcelBuffer."Column No." of
                        1:
                            begin
                                xlNumDoc := TempLExcelBuffer."Cell Value as Text";
                            end;
                        2:
                            begin
                                xlNum := TempLExcelBuffer."Cell Value as Text";
                            end;
                        3:
                            begin
                                Evaluate(xlCant, TempLExcelBuffer."Cell Value as Text");
                            end;
                        4:
                            begin
                                Evaluate(xlfecha, TempLExcelBuffer."Cell Value as Text");
                            end;
                        5:
                            begin
                                rlSalesLine.SetRange("Document Type", pRec."Document Type");
                                rlSalesLine.SetRange("Document No.", xlNumDoc);
                                rlSalesLine.SetRange(Type, rlSalesLine.Type::Item);
                                rlSalesLine.SetRange("No.", xlNum);
                                rlSalesLine.SetRange(Quantity, xlCant);
                                rlSalesLine.SetRange("Planned Delivery Date", xlfecha);
                                if rlSalesLine.FindSet(true, true) then begin
                                    Evaluate(rlSalesLine."Planned Delivery Date", TempLExcelBuffer."Cell Value as Text");
                                    rlSalesLine.Modify(true);
                                end;
                            end;
                    end
                until TempLExcelBuffer.Next() = 0;
            end;
        end else begin
            Error('No se ha podido cargar el fichero');
        end;
    end;


    local procedure ExportarLineasPedidoF()
    var
        TempLExcelBuffer: Record "Excel Buffer" temporary;
        rlSalesLine: Record "Sales Line";
        cNewFchPlanf: Label '·Nueva Fecha entrega planificada·';
        i: Integer;
    begin
        //crer un nuevo libro nuevo en exel
        TempLExcelBuffer.CreateNewBook('Pedidos compra');
        //lo preparas para rellenar las hojas exel 
        //rellenar las cabeceras
        AsignaCeldaF(1, 1, rlSalesLine.FieldCaption("Document No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 2, rlSalesLine.FieldCaption("No."), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 3, rlSalesLine.FieldCaption(Quantity), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 4, rlSalesLine.FieldCaption("Planned Delivery Date"), true, true, TempLExcelBuffer);
        AsignaCeldaF(1, 5, cNewFchPlanf, true, true, TempLExcelBuffer);
        if rlSalesLine.FindSet(false) then begin
            i := 1;
            repeat
                i += 1;
                AsignaCeldaF(i, 1, rlSalesLine."Document No.", false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 2, rlSalesLine."No.", false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 3, Format(rlSalesLine.Quantity), false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 4, Format(rlSalesLine."Planned Delivery Date"), false, false, TempLExcelBuffer);
                AsignaCeldaF(i, 5, Format(rlSalesLine."Planned Delivery Date"), false, false, TempLExcelBuffer);
            until rlSalesLine.Next() = 0;
        end;
        TempLExcelBuffer.WriteSheet('', '', '');
        TempLExcelBuffer.CloseBook();
        TempLExcelBuffer.OpenExcel();
    end;

    local procedure AsignaCeldaF(pFila: Integer; pColumna: Integer; pContenido: Text; pNegrita: Boolean; pItalica: Boolean; var TempLExcelBuffer: Record "Excel Buffer" temporary)

    begin
        TempLExcelBuffer.Init();
        TempLExcelBuffer.Validate("Row No.", pFila);
        TempLExcelBuffer.Validate("Column No.", pColumna);
        TempLExcelBuffer.Insert(true);
        TempLExcelBuffer.Validate("Cell Value as Text", pContenido);
        TempLExcelBuffer.Validate(Bold, pNegrita);
        TempLExcelBuffer.Validate(Italic, pItalica);
        TempLExcelBuffer.Modify(true);
    end;
}

