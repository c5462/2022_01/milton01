/// <summary>
/// otra pagina Extendidaaaaa
/// </summary>
pageextension 50000 "MXCCustumerCardExtC01" extends "Customer Card"
{
    layout
    {
        //añademe al final de (aquiel de)
        addlast(content)
        {
            //los cue__ son los cuadraditos que aparecen.
            group(MXCVacunasC01)
            {
                Caption = 'Vacunas';

                field(MXCNumVacunaC01; Rec.MXCNumVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Numero de vacuna field.';
                    ApplicationArea = All;
                }
                field(MXCFechaUltimaVacunaC01; Rec.MXCFechaUltimaVacunaC01)
                {
                    ToolTip = 'Specifies the value of the Fecha de la ultima vacuna field.';
                    ApplicationArea = All;
                }
                field(MXCTipoVacC01; Rec.MXCTipoVacC01)
                {
                    ToolTip = 'Specifies the value of the tipo de vacuna field.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addlast(Processing)
        {
            action(MXCAsignarVacunaBloqC01)
            {
                ApplicationArea = All;
                Caption = '💉asignar Vacuna Bloqueada';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Acción pulsada');
                    Rec.Validate(MXCTipoVacC01, 'D');//campos asignado con el validate  //asignar de manera dura 
                end;
            }
            action(MXCVariablesGlobalesAppC01)
            {
                ApplicationArea = all;
                Caption = 'Prueba Variables globales a BC';
                trigger OnAction()
                var
                    // cuMXCFuncionesAppC01: Codeunit MXCFuncionesAppC01;
                    newLabel: Label 'El valor actual es %1, lo cambiamos al valor %2';

                begin
                    Message(newLabel, cuMXCFuncionesAppC01.GetSwitchF(), not cuMXCFuncionesAppC01.GetSwitchF());
                    cuMXCFuncionesAppC01.SetSwitchF(not cuMXCFuncionesAppC01.GetSwitchF());
                    //cuando pongo un tanto porcuenro es un parametro
                end;
            }
            action(MXCCopiarEnProveedoresC01)
            {
                Caption = 'Clonar en Proveedores';
                Image = Addresses;
                ApplicationArea = all;
                //si queremos que exista la posiblilidad de ponerlo en los 3 puntitos
                Promoted = true;
                Scope = Repeater;
                trigger OnAction()
                var
                    rlVendor: Record Vendor;

                begin
                    if rlVendor.Get(Rec."No.") then begin
                        Error('El Registro: %1 YA EXISTE en Proveedores', Rec."No.");
                    end else begin
                        rlVendor.Init();
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert(true);
                        Message('Cliente creado correctamente');
                    end;
                end;
            }
        }
    }

    var
        cuMXCFuncionesAppC01: Codeunit "MXCVariablesGlobalesAppC01";
}
