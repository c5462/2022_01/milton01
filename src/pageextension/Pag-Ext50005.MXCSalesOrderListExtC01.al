/// <summary>
/// PageExtension MXCSalesOrderListExtC01 (ID 80105) extends Record Sales Order List.
/// </summary>
pageextension 50005 "MXCSalesOrderListExtC01" extends "Sales Order List"
{
    actions
    {
        addlast(processing)
        {
            action(MXCExportarPedidosC01)
            {
                Caption = 'Exportar Pedidos';
                Image = Export;
                ApplicationArea = All;

                trigger OnAction()

                begin
                    ExportarPedidosVentaF();
                    Message('vamos a exportalo bien');
                end;
            }
            action(MXCImportarPedidosC01)
            {
                Caption = 'ImportarPedidos';
                Image = Import;
                ApplicationArea = All;

                trigger OnAction()

                begin
                    ImportarPedidosVentaF();
                    Message('vamos a exportalo bien');
                end;
            }

        }
    }

    local procedure ExportarPedidosVentaF()
    var
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        //campos 
        XlInStr: Instream;
        //cabecera
        ClCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5%1%6%1%7', Locked = true;
        ClCadenaLinea: Label 'L%1%2%1%3%1%4%1%5%1%6', Locked = true;
        ClSeparador: Label '·', Locked = true;
        ClSeparadorLinea: Label '·', Locked = true;
        xlOutStr: OutStream;
        xlNombreFichero: Text;
    begin
        TempLMXCConfiguracionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::Windows);
        Message('Seleecciona los pedidos a exportar');

        CurrPage.SetSelectionFilter(rlSalesHeader);
        //recorremos cabecera
        if rlSalesHeader.FindSet(false) then begin
            //Message('%1', rlSalesHeader);
            repeat

                xlOutStr.WriteText(StrSubstNo(ClCadenaCabecera, ClSeparador, rlSalesHeader."No.", rlSalesHeader."Document Type", rlSalesHeader."Bill-to Customer No.", rlSalesHeader."Order Date", rlSalesHeader."Sell-to Customer Name", rlSalesHeader.Status, rlSalesLine.Amount));
                xlOutStr.WriteText();

                rlSalesLine.SetRange("Document No.", rlSalesHeader."No.");
                rlSalesLine.SetRange("Document Type", rlSalesHeader."Document Type");
                //recorremoslineas
                if rlSalesLine.FindSet(false) then begin
                    repeat
                        xlOutStr.WriteText(StrSubstNo(ClCadenaLinea, ClSeparadorLinea, rlSalesLine."Document Type", rlSalesLine."No.", rlSalesLine.Description, rlSalesLine."Location Code", rlSalesLine.Quantity));
                        xlOutStr.WriteText();
                    until rlSalesLine.Next() = 0;
                end;
            until rlSalesHeader.Next() = 0;

        end;
        TempLMXCConfiguracionC01.MiBlob.CreateInStream(XlInStr);
        //xlNombreFichero := 'Cabecera.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;

    end;

    local procedure ImportarPedidosVentaF()

    var
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary;
        rlSalesHeader: Record "Sales Header";
        rlSalesLine: Record "Sales Line";
        ///Fuciones de capturar errores
        culCapturaErroresC01: Codeunit MXCFuncionesErroresC01;
        xlFecha: Date;
        xlDecimal: Decimal;
        XlInStr: Instream;
        xNumLinea: Integer;
        ClCadenaLinea: Label 'L%1%2%1%3%1%4%1%5%1%6', Locked = true;
        ClSeparador: Label '·', Locked = true;
        ClSeparadorLinea: Label '-', Locked = true;
        xlOption: Option micasa,hola;
        xlinea: Text;

    begin
        TempLMXCConfiguracionC01.MiBlob.CreateInStream(XlInStr, TextEncoding::Windows);
        if UploadIntoStream('', XlInStr) then begin
            while not XlInStr.EOS do begin //mientras que no hayamos llegado al fin del string EOS_> final del string
                XlInStr.ReadText(xlinea);//leemos la linea, 
                Message(xlinea);
                //Insertar Cabecera o lineas en funcion del 1er caracter de la line
                case true of // case xlinea.Toupper() [1]
                    xlinea[1] = 'C':  //Alta Cabecera
                                      ///xlOutStr.WriteText(StrSubstNo(ClCadenaCabecera, ClSeparador, rlSalesHeader."No.", rlSalesHeader."Document Type", rlSalesHeader."Bill-to Customer No.", rlSalesHeader."Order Date", rlSalesHeader."Sell-to Customer Name", rlSalesHeader.Status, rlSalesLine.Amount));

                        begin//insertar registros
                            rlSalesHeader.Init();
                            rlSalesHeader.Validate("No.", xlinea.Split(ClSeparador).Get(2));
                            Evaluate(rlSalesHeader."Document Type", xlinea.Split(ClSeparador).Get(2));
                            rlSalesHeader.Insert(true);
                            rlSalesHeader.Validate("Bill-to Customer No.", xlinea.Split(ClSeparador).Get(4));
                            if Evaluate(xlFecha, xlinea.Split(ClSeparador).Get(4)) then begin
                                rlSalesHeader.Validate("Order Date", xlFecha);
                            end;
                            rlSalesHeader.Validate("Sell-to Customer Name", xlinea.Split(ClSeparador).Get(5));
                            rlSalesHeader.Modify(true);

                            if culCapturaErroresC01.Run() then begin
                                Message('El boton dice que se esta ejecutando');


                            end else begin
                                Message('NO se ha podido ejecutar la code unit :%1 ', GetLastErrorText());
                            end;


                        end;

                    xlinea[1] = 'L':
                        //ALta linea
                        ///xlOutStr.WriteText(StrSubstNo(ClCadenaLinea, ClSeparadorLinea, rlSalesLine."Document Type", rlSalesLine."No.", rlSalesLine.Description, rlSalesLine."Location Code", rlSalesLine.Quantity));

                        begin
                            rlSalesLine.Init();
                            rlSalesLine.Validate("No.", xlinea.Split(ClSeparadorLinea).Get(3));
                            if Evaluate(xNumLinea, xlinea.Split(ClSeparadorLinea).Get(2)) then begin
                                rlSalesLine.Validate("Line No.", xNumLinea);
                            end;
                            rlSalesLine.Insert(true);

                            if Evaluate(xlOption, xlinea.Split(ClCadenaLinea).Get(3)) then begin
                                rlSalesLine.Validate("Document Type", xlOption);
                            end;
                            rlSalesLine.Validate(Description, xlinea.Split(ClSeparadorLinea).Get(4));
                            rlSalesLine.Validate("Location Code", xlinea.Split(ClSeparadorLinea).Get(5));
                            if Evaluate(xlDecimal, xlinea.Split(ClSeparadorLinea).Get(6)) then begin
                                rlSalesLine.Validate(Quantity, xlDecimal);


                                rlSalesLine.Modify(true);
                            end;

                            rlSalesLine.Modify(true);
                        end;

                    else
                        Error('TPO de linea %1 no reconocido', xlinea[1]);
                end;
            end;
        end else begin
            Error('No se ha podido subir el fichero al servidor');
        end;
    end;
}
