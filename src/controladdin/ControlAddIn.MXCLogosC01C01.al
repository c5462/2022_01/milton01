/// <summary>
/// ControlAddIn "MXCLogosC01.C01"
/// </summary>

controladdin MXCLogoMiniC01
{
    Images = 'Logo/LogoMXC.png';
    VerticalStretch = true;
    VerticalShrink = true;
    HorizontalStretch = true;
    HorizontalShrink = true;
    RequestedHeight = 804;
    StartupScript = 'src/controladdin/MXCLogosC01/LogoMenus.js';
}