
/// <summary>
/// Extension de la tabla de prooveedores para provar nuestro codeunit
/// </summary>
tableextension 50001 "MXCVendorC01" extends Vendor
{
    trigger OnAfterInsert()

    //variable gl
    var
        cuMXCVariablesGlobalesAppC01: Codeunit MXCVariablesGlobalesAppC01;
    begin
        //insertamos en el campo "name" nuestra variable global almacenada en el codeunit
        Rec.Validate(Name, cuMXCVariablesGlobalesAppC01.NombreF());
    end;

}
