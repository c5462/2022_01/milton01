/// <summary>
/// Extension de la tabla de Clientes para añadir campos , en este caso  el campo de Vacuna
/// </summary>
tableextension 50000 "MXCCustomerExtC01" extends Customer
{

    fields
    {
        field(50000; MXCNumVacunaC01; Integer)
        {
            Caption = 'Numero de vacuna';
            //  DataClassification = CustomerContent;
            FieldClass = FlowField;
            //CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            //Editable = false;

            CalcFormula = count("MXCLinPlanVacunacionC01" where(ClientetoVacuna = field("No.")));


        }
        field(50001; MXCFechaUltimaVacunaC01; Date)
        {
            //campo calculado
            FieldClass = FlowField;
            Caption = 'Fecha de la ultima vacuna';
            Editable = false;
            CalcFormula = max("MXCLinPlanVacunacionC01".FechaVacunacion where(ClientetoVacuna = field("No.")));
            //  DataClassification = CustomerContent;

        }
        field(50002; MXCTipoVacC01; Code[20])
        {
            Caption = 'tipo de vacuna';
            DataClassification = CustomerContent;
            TableRelation = MXCVariosC01.Codigo where(Tipo = const(Vacuna));

            //triger en campo
            trigger OnValidate()
            //variables locales 
            var
                rlMXCVariosC01: Record MXCVariosC01;
            begin
                //traete la vacuna que ha escrito el cliente , en este caso devolvera un boleano.

                if (rlMXCVariosC01.Get(rlMXCVariosC01.Tipo::Vacuna, Rec.MXCTipoVacC01)) then begin
                    //hace el test que si tiene el campo de falso activado pues te lo muesra 
                    //Tesfiel sin segundo paramtro comprobara si hay algo 
                    rlMXCVariosC01.TestField(bloqueado, false);
                end;

                // if rlMXCVariosC01.FindSet(MXCTipoVacC01);
            end;
        }
        //modify (nombre de campo0)
        modify(Name)
        {
            trigger OnAfterValidate()
            var
                culMIMXCVariablesGlobales: Codeunit MXCVariablesGlobalesAppC01;

            begin

                culMIMXCVariablesGlobales.NombreF(Rec.Name);
            end;
        }



    }

    trigger OnAfterInsert()
    begin
        //filtar por empresas distintas a la que estamos 
        SincronizarRegistrosF(xAccion::Insert);
    end;

    trigger OnAfterModify()

    begin
        SincronizarRegistrosF(xAccion::Modify);
    end;

    trigger OnDelete()

    begin
        SincronizarRegistrosF(xAccion::Delete);
    end;

    local procedure SincronizarRegistrosF(pAccion: Option)
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        rlCompany.SetFilter(Name, '<>%1', CompanyName);
        //rcorrer las empresas
        if rlCompany.FindSet(false) then begin
            ///no tiene parametros , si no lo vamos a modificar false.
            repeat
                //cambiar la variable local de registro de clientes
                rlCustomer.ChangeCompany(rlCompany.Name);
                //asgnar los valores de cliente a la variable local
                rlCustomer.Init();
                rlCustomer.Copy(Rec);
                case pAccion of
                    xAccion::Insert:
                        rlCustomer.Insert(false);
                    //insertar el cliente
                    // rlCustomer.Insert(false);//sin true para que no cree os contactos ,ya que los crea en la empresa 
                    xAccion::Modify:
                        rlCustomer.Modify(false);
                    xAccion::Delete:
                        rlCompany.Delete(false);
                end;
            until rlCompany.Next() = 0;
        end;
    end;



    var
        xAccion: Option Insert,Modify,Delete;


}
