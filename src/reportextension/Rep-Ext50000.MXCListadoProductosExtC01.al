/// <summary>
/// Extension de un report
/// </summary>
reportextension 50000 "MXCListadoProductosExtC01" extends "Sales - Shipment"
{
    dataset
    {
        addlast("Sales Shipment Header")
        {
            dataitem(MXCSalesShipmentHeaderC01;
            "Sales Shipment Header")
            {


                column(MXCShiptoName_SalesShipmentHeaderC01; "Ship-to Name")
                {
                }
                column(MXCShiptoAddress_SalesShipmentHeaderC01; "Ship-to Address")
                {
                }
                column(MXCShiptoCity_SalesShipmentHeaderC01; "Ship-to City")
                {
                }
            }

        }
    }
}
