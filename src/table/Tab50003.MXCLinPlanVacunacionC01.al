/// <summary>
/// Tabla de lin
/// </summary>
table 50003 "MXCLinPlanVacunacionC01"
{

    Caption = 'Lineas Plan Vacunacion';
    DataClassification = SystemMetadata;
    //LookupPageId = MXCSubPageLinPlanVacunacioC01;
    DrillDownPageId = MXCSubPageLinPlanVacunacioC01;

    fields
    {
        field(1; CodLineas; Code[20])
        {

            Caption = 'Codt';
            //  Editable = false;
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; NumLinea; Integer)
        {
            // Editable = false;
            Caption = 'NumLinea';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; ClientetoVacuna; Code[20])
        {
            TableRelation = Customer."No.";
            Caption = 'ClientetoVacuna';
            DataClassification = OrganizationIdentifiableInformation;


            trigger OnValidate()
            //variables locales 
            var
                rlMXCPLanVacunacionMXCC01: Record MXCPllanVacunacionC01;

            //rlFecaa: Record MXCPllanVacunacionC01;
            begin

                if Rec.FechaVacunacion = 0D then begin
                    if rlMXCPLanVacunacionMXCC01.Get(Rec.CodLineas) then begin
                        Rec.Validate(FechaVacunacion, rlMXCPLanVacunacionMXCC01.FechaIniPlanificacion);
                    end;
                end;

                //if Rec.FechaVacunacion = 0

            end;
        }
        field(4; FechaVacunacion; Date)
        {
            Caption = 'Fecha de la Vacunacion';
            DataClassification = OrganizationIdentifiableInformation;
            trigger OnValidate()
            begin

                CalcularFechProxVacuF();
            end;
        }
        field(5; CodVacuna; Code[20])
        {
            TableRelation = MXCVariosC01.Codigo where(Tipo = const(Vacuna), bloqueado = const(false));

            Caption = 'Tipo vacuna';
            DataClassification = OrganizationIdentifiableInformation;

            trigger OnValidate()
            begin

                CalcularFechProxVacuF();
            end;
        }
        field(6; CodOtros; Code[20])
        {
            TableRelation = MXCVariosC01.Codigo where(Tipo = const(Otros), bloqueado = const(false));
            Caption = 'Tipo otros';
            DataClassification = SystemMetadata;
        }
        field(7; Fecha2Vacunacion; Date)
        {
            Caption = 'Fecha 2º Vacunación';
            DataClassification = SystemMetadata;
            //TableRelation=MXCVariosC01.PeridoSegundaVacunacion
            // trigger OnValidate()
            // //variables 
            // var
            //     rlFecha2Vac: Record MXCVariosC01;

            // begin
            //     Rec.Validate(Fecha2Vacunacion, fecha2VacF());
            // end;



        }

    }
    keys
    {
        key(PK; CodLineas, NumLinea)
        {
            Clustered = true;
        }
    }


    //    procedure NombreEmpresaVacunadoraF(): Text
    //     var
    //         rlVendor: Record Vendor;
    //     begin
    //         //if rlCustomer.get(Rec.CodCteWeb)
    //         if rlVendor.get(Rec.EmpresaVacunadora)
    //         then begin
    //              exit(rlVendor.Name);
    //         end;
    //     end;
    // 

    /// <summary>
    /// Ndevuelve el codigo cliente a vacunar
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreClientetoVacunaF(): Text

    var
        rlCustumer: Record Customer;
    begin
        //if rlCustomer.get(Rec.CodCteWeb)
        if rlCustumer.get(Rec.ClientetoVacuna)
        then begin
            exit(rlCustumer.Name);
        end;
    end;

    /// <summary>
    /// DesVariosF.
    /// </summary>
    /// <param name="pTipo">Enum MXCTipoC01.</param>
    /// <param name="pCodVarios">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    ///
    procedure DesVariosF(pTipo: Enum MXCTipoC01; pCodVarios: Code[20]): Text
    var
        rlVarrios: Record MXCVariosC01;
    begin
        if rlVarrios.Get(pTipo, pCodVarios) then begin
            exit(rlVarrios.Descripcion);
        end;
    end;


    /// <summary>
    /// funcion que devulve la suma de la fcha y el dateFormula
    /// </summary>
    procedure CalcularFechProxVacuF()
    var
        rlMXCvariosC01: Record MXCVariosC01;
        IsHandled: Boolean;
    begin
        OnBeforeCalcularFechaProximaVacunaF(Rec, xRec, rlMXCvariosC01, IsHandled, CurrFieldNo);
        if not IsHandled then begin

            if Rec.FechaVacunacion <> 0D then begin
                if rlMXCvariosC01.Get(rlMXCvariosC01.Tipo::Vacuna, Rec.CodVacuna) then begin
                    Rec.Validate(Fecha2Vacunacion, CalcDate(rlMXCvariosC01.PeridoSegundaVacunacion, Rec.FechaVacunacion));
                end;
            end;
        end;
        OnAfterCalcularFechaProximaVacunaF(Rec, xRec, rlMXCvariosC01, CurrFieldNo);
    end;


    //publicar 
    [IntegrationEvent(false, false)]
    local procedure OnBeforeCalcularFechaProximaVacunaF(Rec: Record "MXCLinPlanVacunacionC01"; xRec: Record "MXCLinPlanVacunacionC01"; rlMXCvariosC01: Record MXCVariosC01; var IsHandled: Boolean; pCurFieldNo: Integer)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularFechaProximaVacunaF(Rec: Record "MXCLinPlanVacunacionC01"; xRec: Record "MXCLinPlanVacunacionC01"; rlMXCvariosC01: Record MXCVariosC01; CurrFieldNo: Integer)
    begin
    end;


    /*
        [BusinessEvent(false)]
        local procedure OnAfterCalcularFechaProximaVacunaF()
        begin
        end;

        [IntegrationEvent(false, false)]
        local procedure OnBeforeCalcularFechaProximaVacunaF()
        begin
        end;
    */
    // procedure fecha2VacF(): Date
    // //calDAte (DateExpreson ,deja)
    // var
    //     rFech: Record MXCVariosC01;
    // begin



    //     if rFech.Get(Fecha2Vacunacion) then begin
    //         exit(CalcDate(rFech.))
    //     end;

    //     //CalcDate()



    // end;


    //






}
