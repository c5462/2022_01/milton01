/// <summary>
/// Tabla para guardar los log
/// </summary>
table 50004 "MXCTablaLogC01"
{
    Caption = 'TablaLogC01';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; Id; Guid)
        {
            Caption = 'Id';
            DataClassification = OrganizationIdentifiableInformation;



        }
        field(2; Mensaje; Text[250])
        {
            Caption = 'Mensaje';
            DataClassification = OrganizationIdentifiableInformation;
        }

    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
        key(creado; SystemCreatedBy)
        {
            //Cluster =trueF
        }

    }
    trigger OnInsert()
    begin
        if IsNullGuid(Id) then begin
            Rec.Validate(Id, CreateGuid());
        end;

    end;


}
