/// <summary>
/// Es una tabla de repaso de 
/// </summary>
table 50005 "MXCRepasoBCC01"
{
    Caption = 'Tabla repaso BC Milton';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; Id; Code[20])
        {
            Caption = 'Id de registro';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; CodCte; Code[20])
        {
            Caption = 'Código Cliente ';
            DataClassification = SystemMetadata;
        }
        field(3; TextoReg; Code[50])
        {
            Caption = 'Texto Registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum MXCTYpeDocC01)
        {
            Caption = 'Texto Registro';
            DataClassification = SystemMetadata;
        }




    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
}
