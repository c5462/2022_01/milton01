/// <summary>
/// Esta es la primera aplicacion que hemos hecho en el curso 
/// </summary>
table 50000 "MXCConfiguracionC01"
{
    Caption = 'Configuración de la app 01 del curso';
    DataClassification = OrganizationIdentifiableInformation;
    /*las propiedades  mas usadas
   -Caption es lo que el usuario va ha ver
   -DataCaptionFields  es el que asocia 
   -Extensible ,por defecto activado
   -LookupPageId, mirar la pagina que tiene que ejecutar.
   -Permissions
   -TableType: crm ó cds, exchange, microsftGrap, temporal(No se crean en SQL),ExternalSQL
   */
    /*----CAMPOS--*/

    /*LAs propiedads de campos 
     son b casi punteros media(un archivo , ref a un GUID) y media set (varias archivos,ref a los GUID).
     -Los datos de DateTime,
     */
    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id de registro';
            //-CaptionClass */Es un campo dimanico que cambia conforme se ejecute eo navegue         
            DataClassification = SystemMetadata;
            //TableRelation = MXCConfiguracionC01.Id;
        }
        field(2; CodCteWeb; Code[20])
        {
            Caption = 'Código cliente para Web';
            DataClassification = SystemMetadata;
            //-TableRelation:Relacion con una tabla =/=TestTableRelation
            TableRelation = Customer."No."; //TableRelation a la tabla de clientes (hacemos un lookup a dicha tabla)
            //TestTableRelation = true; //me dice que esta opsoleto en esta version
        }
        field(3; TextoRegistro; Text[250])
        {
            Caption = 'Texto registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum MXCTipoDocC01)
        {
            Caption = 'Tipo de Documento';
            DataClassification = SystemMetadata;
            /*variables tipo registro de tablas*/
            // Clear(Nombre de la tabla.porc=0) 
            //Custumer.init();

        }
        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre del cliente ';
            //DataClassification = CustomerContent;
            FieldClass = FlowField;//no tienen mucho sentido 
            CalcFormula = lookup(Customer.Name where("No." = field(CodCteWeb)));
            Editable = false;
        }

        //un campo tipo de tabla option , cliente , empleado, recurso,
        field(6; TipoTabla; Enum MXCTipoTablaC01)
        {
            Caption = 'Tipo de tabla';
            DataClassification = SystemMetadata;
            //Trigger de campo, solo se ejecuta si lo introducido por el usuario es distinto de lo que había
            //unicamente por pantalla , asigna a ese valor que haga un trigger 
            trigger OnValidate()
            begin

                if xRec.TipoTabla <> Rec.TipoTabla then begin
                    //cuando estamos haciendo asignaciones nunca := (variables), !!mejor usar Validate a valores de Campo!!!
                    //asignacion a variables :=  Rec.CodTabla := '';    
                    Rec.Validate(CodTabla, '');//Le insertamos un Blank al campo CodTabla
                end;
            end;


        }

        //la Londuitud de Code es de <<<<<<<<<20>>>>>>>>>
        field(7; CodTabla; Code[20])
        {
            Caption = 'CodTabla';
            DataClassification = SystemMetadata;
            //Cont //filter, cliente, cuando solo hay un valor hay que ponerle una const
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            //me faltaba  un enum de contacto
            if (TipoTabla = const(Contacto)) Contact else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource;

        }
        field(8; ColorFondo; Enum MXCColoresC01)
        {
            Caption = 'Color fondo';
            DataClassification = SystemMetadata;
        }
        field(9; ColorLetra; Enum MXCColoresC01)
        {
            Caption = 'Color letra';
            DataClassification = SystemMetadata;

        }
        field(10; Cod2; Text[20])
        {
            Caption = 'Código 2';
            DataClassification = SystemMetadata;
        }

        field(11; UdsDisponibles; Decimal)
        {
            Caption = 'Nº de unidades disponibles';
            FieldClass = FlowField;
            //vamos hacer una suma de 
            CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto), "Posting Date" = field("filtro fecha"), "Entry Type" = field(FiltroTipoMov)));

        }
        //caundo le digamos 
        //este tipo de campos no hay en la bbdd , como no exxistiera.
        field(12; FiltroProducto; Code[20])
        {
            //como tiene relcion con otra tabla 
            TableRelation = Item."No.";
            Caption = 'Filtro producto';
            FieldClass = FlowFilter;
        }
        field(13; "filtro fecha"; Date)
        {

            Caption = 'Filtrofecha';
            FieldClass = FlowFilter;
        }
        //tipo de movimiento
        field(14; FiltroTipoMov; Enum "Item Ledger Entry Type")
        {
            Caption = 'Tipo de movimiento';

            FieldClass = FlowFilter;
        }
        field(15; IdPassword; Guid)
        {
            DataClassification = SystemMetadata;
            //Caption = 'Password general';   // no se va a mostrar
            //FieldClass = FlowFilter;
            //TableRelation = "Item Ledger Entry"."Entry Type";

        }
        field(16; MiBlob; Blob)
        {
            //no se
            DataClassification = SystemMetadata;
        }
        field(17; FiltroTipoMov2; Enum "Item Ledger Entry Type")
        {
            Caption = 'Tipo de movimiento';

            FieldClass = FlowFilter;
        }

    }
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    /*
    fieldgroups{
        fieldgroup(DropDown,);
          fieldgroup(Brick,);
    }

    */

    //Creacion de una funcion y no procedimiento , funcion es encapsulable y te devuelve un valor 
    //procedure = funcion, los nombres de  funciones terminan con "F", se usa nomenclatura !!!PASCALCASE!!!!
    /// <summary>
    /// Recupera el registro de la tabla act.y sino exixte lo crea.
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        //si no tiene regisro se va a crear 
        if not Rec.get() then begin
            Rec.Init();
            Rec.Insert(true);
        end;
        exit(true);
    end;

    //la utilidad de las funciones es reutilizar y documentarlo bien .
    /// <summary>
    /// Devuelve el nombre del cliente pasado por paramereo . Si no existe devolverá un texto vacío.
    /// </summary>
    /// <param name="pCodCte"></param>
    /// <returns></returns>
    procedure NombreClienteF(pCodCte: Code[20]): Text
    //a las variables locales se le tienen tienen que distinguir con una l
    //variables locales se colocan entre prcedure y begin >>>los Registros con r al principio
    //los parameros una p delante de lo que se le pasa por parametro
    var
        rlCustomer: Record Customer;
    begin
        //if rlCustomer.get(Rec.CodCteWeb)
        if rlCustomer.get(pCodCte)
        then begin
            exit(rlCustomer.Name);
        end;
    end;

    /// <summary>
    /// NombreTablaF.
    /// </summary>
    /// <param name="pTipoTabla">Enum MXCTipoTablaC01.</param>
    /// <param name="pCodTabla">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure NombreTablaF(pTipoTabla: Enum MXCTipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlMXCConfiguracionC01: Record MXCConfiguracionC01;
        rlContact: Record Contact;
        rlEmploye: Record Employee;
        rlRecurso: Record Resource;
        rlVendor: Record Vendor;


    begin
        //un sticwh es un case cuando hay muchos valores , mas rapidos con los if
        case
            pTipoTabla of
            //para asignar el valor que tiene. :: 
            pTipoTabla::Cliente:
                begin
                    exit(rlMXCConfiguracionC01.NombreClienteF(pCodTabla));
                end;
            pTipoTabla::Contacto:
                begin
                    if rlContact.get(pCodTabla) then begin
                        exit(rlContact.Name);
                        //exit(Rec.NombreClienteF(pCodTabla));
                    end;
                end;

            pTipoTabla::Empleado:
                begin
                    if rlEmploye.get(pCodTabla) then begin
                        exit(rlEmploye.FullName());
                        //exit(Rec.NombreClienteF(pCodTabla));

                    end;
                end;
            pTipoTabla::Recurso:
                begin
                    if rlRecurso.get(pCodTabla) then begin
                        exit(rlRecurso.Name);
                        //exit(Rec.NombreClienteF(pCodTabla));
                    end;
                end;
            pTipoTabla::Proveedor:
                begin
                    if rlVendor.get(pCodTabla) then begin
                        exit(rlVendor.Name);
                        //exit(Rec.NombreClienteF(pCodTabla));
                    end;
                end;

        end;
    end;

    /// <summary>
    /// PasswordF.
    /// </summary>
    /// <param name="pPassword">Text.</param>
    [NonDebuggable]
    procedure PasswordF(pPassword: Text)
    begin
        //No haria falta un init porque ya esta incrustado
        Rec.Validate(IdPassword, CreateGuid());
        Rec.Modify(true);
        IsolatedStorage.Set(Rec.IdPassword, pPassword, DataScope::Company)
        // almacena en una zona especial de SQL a la que no se puede acceder con la clave GUID guardada, 
        // en la base de datos se guarda el Guid, no el password

    end;

    [/// <summary>
     /// PasswordF.
     /// </summary>
     /// <returns>Return variable xSalida of type Text.</returns>
    NonDebuggable]// lo que hace esto es no permitir el debug en esta funcion 
    procedure PasswordF() xSalida: Text
    begin
        if not IsolatedStorage.get(Rec.IdPassword, DataScope::Company, xSalida) then begin

            Error('No se encuengra el password');
        end;

    end;


}

