/// <summary>
/// Table MXCCustomerC01 (ID 80109).
/// </summary>
table 50007 MXCCarModelC01
{
    Caption = 'Car Model';
    DataClassification = CustomerContent;
    fields
    {
        field(1; Name; Text[100])
        {
            Caption = 'Name';
        }
        field(2; Description; Text[100])
        {
            Caption = 'Description';
        }
        field(3; "Brand Id"; Guid)
        {
            TableRelation = MXCCarBrandC01.SystemId;
            Caption = 'Brand Id';
        }
        field(4; Power; Integer)
        {
            Caption = 'Power (cc)';
        }
        field(5; "Fuel Type"; Enum MXCFuelTypeC01)
        {
            Caption = 'Fuel Type';
        }
    }
    keys
    {
        key(PK; Name, "Brand Id")
        {
            Clustered = true;
        }
    }
}