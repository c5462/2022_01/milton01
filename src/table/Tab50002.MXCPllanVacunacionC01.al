/// <summary>
/// cabecera del plan de vacunacion.
/// </summary>
table 50002 "MXCPllanVacunacionC01"
{
    Caption = 'PllanVacunacionC01';
    DataClassification = SystemMetadata;
    LookupPageId = MXCListaPLanVacunacionC01;

    fields
    {
        field(1; CodCabeceras; Code[20])
        {
            Caption = 'Codt';
            DataClassification = SystemMetadata;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripcion';
            DataClassification = SystemMetadata;
        }
        field(3; FechaIniPlanificacion; Date)
        {
            Caption = 'FechaIniPlanificacion';
            DataClassification = SystemMetadata;
        }
        field(4; EmpresaVacunadora; Code[20])
        {
            Caption = 'EmpresaVacunadora';
            DataClassification = SystemMetadata;
            TableRelation = Vendor."No.";
        }
        field(5; MiBlob; Blob)
        {
            DataClassification = SystemMetadata;
        }

    }
    keys
    {
        key(PK; CodCabeceras)
        {
            Clustered = true;
        }
    }


    //   procedure NombreEmpresaVscunadora(pCodCte: Code[20]): Text


    //a las variables locales se le tienen tienen que distinguir con una l
    //variables locales se colocan entre prcedure y begin >>>los Registros con r al principio
    //los parameros una p delante de lo que se le pasa por parametro

    /// <summary>
    /// funcion que devuelve
    /// </summary>
    /// <returns>Return value of type Text[20].</returns>
    procedure NombreEmpresaVacunadoraF(): Text
    var
        rlVendor: Record Vendor;
    begin
        //if rlCustomer.get(Rec.CodCteWeb)
        if rlVendor.get(Rec.EmpresaVacunadora)
        then begin
            exit(rlVendor.Name);
        end;
    end;

    trigger OnDelete()
    ///que es lo que quiero borrar
    var
        rlMXCLinPlanVacunacionMXCC01: Record "MXCLinPlanVacunacionC01";
    begin
        rlMXCLinPlanVacunacionMXCC01.SetRange(CodLineas, Rec.CodCabeceras);
        rlMXCLinPlanVacunacionMXCC01.DeleteAll(true);
        // if  rlMXCLinPlanVacunacionMXCC01.FindSet(true) then begin
        //     rlMXCLinPlanVacunacionMXCC01.DeleteAll(true);
        // end;
    end;

}
