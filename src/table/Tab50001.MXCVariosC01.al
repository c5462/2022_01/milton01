/// <summary>
/// la tabla varios de vaxcionas
/// </summary>
table 50001 "MXCVariosC01"
{
    Caption = 'Varios';
    DataClassification = SystemMetadata;
    LookupPageId = "MXCFichaVariosC01";
    DrillDownPageId = "MXCListaVariosC01";
    fields
    {
        field(1; Tipo; Enum MXCTipoC01)
        {
            Caption = 'Tipo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; bloqueado; Boolean)
        {
            Caption = 'Blq.Campo';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(5; PeridoSegundaVacunacion; DateFormula)
        {
            DataClassification = OrganizationIdentifiableInformation;
            Caption = 'segunda vacunacion';
        }
    }
    keys
    {
        key(PK; Tipo, Codigo)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion)
        {
        }
    }

    trigger OnDelete()
    begin
        //no deja borrar si no esta bloq
        /*
        if not  Rec.bloqueado then begin
            Error('No se permite eliminar un registro no bloquead');
        end;

        */
        //hace u tesfiel de que si esta bloq pues deja borrar
        Rec.TestField(bloqueado, true);
    end;



}
