/// <summary>
/// Table MXCRegistrosTotalesC01 (ID 80106).
/// </summary>
table 50006 "MXCRegistrosTotalesC01"
{
    Caption = 'RegistrosTotalesC01';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; IDEmpre; Text[30])
        {
            Caption = 'IDEmpre';
            DataClassification = SystemMetadata;
            TableRelation = Company.Name;
        }
        field(2; Clientes; Integer)
        {
            Caption = 'Clientes';

            FieldClass = FlowField;
            CalcFormula = count(Customer);
        }
        field(3; Proveedores; Integer)
        {
            Caption = 'Proveedores';
            FieldClass = FlowField;
            CalcFormula = count(Vendor);
        }
        field(4; Productos; Integer)
        {
            Caption = 'Productos';
            FieldClass = FlowField;
            CalcFormula = count(Item);
        }
    }
    keys
    {
        key(PK; IDEmpre)
        {
            Clustered = true;
        }
    }

    /// <summary>
    /// Instancia el nombre de la empresa
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreEmpesaF(): Text
    var
        rlcompany: Record "Company";
    begin
        if rlcompany.get() then begin
            exit(rlcompany.Name)
        end;
    end;
}
