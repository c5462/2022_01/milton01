/// <summary>
/// Vamos hacer una suscricion
/// </summary>
codeunit 50003 "MXCSuscripcionesC01"
{
    //[EventSubscriber(ObjectType::Codeunit, Codeunit::, 'OnSomeEvent', 'ElementName', SkipOnMissingLicense, SkipOnMissingPermission)]
    //SkipOnMissingLicense=false, SkipOnMissingPermission=false
    //permiten que de un error si el usuario no tienen licencia 

    SingleInstance = true;

    //Es mejor declarar Suscripciones  con un sniper

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No.", '');
        //le estamos diciendo que eso tenga algun dato para rellenar

    end;

    [EventSubscriber(ObjectType::Table, Database::"Record Link", 'OnAfterValidateEvent', 'Description', false, false)]
    local procedure DatabaseRecordLinkOnAfterValidateEventF(CurrFieldNo: Integer; var Rec: Record "Record Link"; var xRec: Record "Record Link")
    begin
        //confirm(Txto),defecto,valio  y deveulve un boolean 
    end;

    //
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    var
        cuMXCSuscripcionesC01: Codeunit MXCFuncionesAppcC01;
    begin
        cuMXCSuscripcionesC01.MensajeFactCompraF(PurchInvHdrNo);

    end;

    ///

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        culMXCFuncionesAppcC01: Codeunit MXCFuncionesAppcC01;
    begin
        //aqui llamamos al a la funcion de la codeunit
        culMXCFuncionesAppcC01.CodeunitSalesPostOnAfterPostSalesDocvarSalesHeaderRecordSaleHeaderF(SalesHeader);

    end;







}