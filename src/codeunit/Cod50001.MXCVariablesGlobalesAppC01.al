/// <summary>
/// code uni
///
/// </summary>
codeunit 50001 "MXCVariablesGlobalesAppC01"
{
    SingleInstance = true;//MUY IMPORTANTE!!!! Para que mantenga el valor de las variables globales entre objetos diferentes. No hay que abusar de esto, usar solo en este tipo de Codeunit y en la Codeunit suscriptora

    /// <summary>
    /// se le pasara un parametro que lo guardará hasta que se termine la session.
    /// </summary>
    /// <param name="pValor">Boolean.</param>
    procedure SetSwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;

    /// <summary>
    /// nos devolvera el valor que tiene nuestro xswitch
    /// </summary>
    /// <returns>Return variable xSalida of type Boolean.</returns>
    procedure GetSwitchF() xSalida: Boolean
    begin
        xSalida := xSwitch;
    end;

    /// <summary>
    /// es un Settt,guarda el nombre del cliente  a nivel global de BC
    /// </summary>
    /// <param name="pName">Text[100].</param>
    procedure NombreF(pName: Text[100])
    begin
        //Error('Procedure SetNombreF not implemented.');
        xNombre := pName;
    end;

    /// <summary>
    /// es un gettt, devuleve el nombre guardado en BC
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreF(): Text
    begin
        //Error('Procedure SetNombreF not implemented.');
        exit(xNombre);
    end;


    //-------- VARIABLES GLOBALES A TODO BC (porque es SingleInstance) --------//

    var
        xSwitch: Boolean;
        xNombre: Text;
}