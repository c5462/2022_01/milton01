/// <summary>
/// funciones 
/// </summary>
codeunit 50004 "MXCFuncionesAppcC01"
{
    //Función para obtener si un código está dentro de un filtro pasado.
    /// <summary>
    /// DentroDeUnFiltroF.
    /// </summary>
    /// <param name="pCodigo">Code[20].</param>
    /// <param name="pFiltro">Text.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure DentroDeUnFiltroF(pCodigo: Code[20]; pFiltro: Text): Boolean
    var
        TempLCustomer: Record Customer temporary;
    //xlFilterPageBuilder: FilterPageBuilder;
    begin
        TempLCustomer.Init();
        TempLCustomer."No." := pCodigo;
        TempLCustomer.Insert(false);
        TempLCustomer.SetFilter("No.", pFiltro);
        exit(not TempLCustomer.IsEmpty);
    end;

    /// <summary>
    /// procedure CodeunitSalesPostOnAfterPostSalesDocvarSalesHeaderRecordSaleHeaderF.
    /// </summary>
    /// <param name="SalesHeader">VAR Record "Sales Header".</param>

    procedure CodeunitSalesPostOnAfterPostSalesDocvarSalesHeaderRecordSaleHeaderF(var SalesHeader: Record "Sales Header")
    var
        rlCustomer: Record Customer;
        rlSalesLine: Record "Sales Line";
        //   pglCustomerCard: Page "Customer Card";
        xlNotificacion: Notification;
        clienteVacuna: Label 'Cliente %1 no tiene Vacuna';
    //2__VErificar si tiene Vacuna
    begin
        xlNotificacion.Message('El cliente no tiene vacuna');
        if rlCustomer.get(SalesHeader."Bill-to Customer No.") then begin
            //si tiene tipo vacuna
            if rlCustomer.MXCTipoVacC01 = '' then begin
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo(clienteVacuna, rlCustomer.Name));
                xlNotificacion.AddAction('Abrir ficha clientes', Codeunit::MXCFuncionesAppcC01, 'AbrirFichaClienteF');
                //enviar un dato 
                xlNotificacion.SetData('CodCte', rlCustomer."No.");
            end;

        end;
        //3__verificar sus lineas superan los 100 euros 
        rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
        rlSalesLine.SetRange("Document No.", SalesHeader."No.");
        rlSalesLine.SetFilter(Amount, '<%1', 100);//verifica que los que no superan los 100 €
        if rlSalesLine.FindSet(false) then begin
            repeat
                Clear(xlNotificacion);
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo('bb'));
                xlNotificacion.Send();
            until rlCustomer.Next() = 0;
        end;





    end;

    /// <summary>
    /// AbrirFichaClienteF.
    /// </summary>
    /// <param name="pNotificaon">Notification.</param>
    procedure AbrirFichaClienteF(pNotificaon: Notification)
    var
        rlCustomer: Record Customer;
        CodigoCliente: Label 'CodCte';
    begin
        if pNotificaon.HasData(CodigoCliente) then begin
            if rlCustomer.get(pNotificaon.GetData(CodigoCliente)) then begin
                Page.Run(Page::"Customer Card", rlCustomer);
            end;
        end;



    end;

    /// <summary>
    /// AbrirLaListaCtesF.
    /// </summary>
    /// <param name="pNotificaon">Notification.</param>
    procedure AbrirLaListaCtesF(pNotificaon: Notification)
    var
        rlCustomer: Record Customer;
        CodigoCliente: Label 'CodigoCliente';
    begin
        if pNotificaon.HasData(CodigoCliente) then begin
            rlCustomer.SetRange("No.", pNotificaon.GetData(CodigoCliente));
        end;
        Page.Run(Page::"Customer List", rlCustomer);
    end;
    /// <summary>
    /// MensajeFactCompraF
    /// </summary>
    /// <param name="PurchInvHdrNo">VAR Code[20].</param>
    procedure MensajeFactCompraF(var PurchInvHdrNo: Code[20])
    begin
        if PurchInvHdrNo <> '' then begin
            Message('El usuario %1 con factura  %2', UserId, PurchInvHdrNo);
        end;
    end;


    //vamos a poner cosas 
    //MIrcrosf recomienda que las funciones sean locales, las funciones devuelven algo
    /// <summary>
    /// Funcion para hacer cositas
    /// </summary>
    /// <param name="pCodClt">Code [20].</param>
    /// <param name="pParm2">Integer.</param>
    /// <param name="pCustomer">VAR Record "Customer".</param>
    /// <returns>Return variable xSalida of type Text.</returns>
    procedure FuncionesEjemploF(pCodClt: Code[20]; pParm2: Integer; var pCustomer: Record "Customer") xSalida: Text
    begin

    end;
    //con Var entra y modifia osea que es una variable entrada salida
    /// <summary>
    /// EjmVariablesE_SF.
    /// </summary>
    /// <param name="pTexto">VAR Text.</param>
    procedure EjmVariablesE_SF(var pTexto: Text)
    begin
        pTexto := 'Tecon Servicios';
    end;
    //un objeto es 
    /// <summary>
    /// EjmVariablesE_SF02.
    /// </summary>
    /// <param name="pTexto">TextBuilder.</param>
    procedure EjmVariablesE_SF02(pTexto: TextBuilder)
    begin
        pTexto.Append('Tecon Servicios');
    end;
    //
    /// <summary>
    /// EjemploArrayF.
    /// </summary>
    procedure EjemploArrayF()
    var
        mlNumeros: array[20] of Text;
        i: Integer;
    // xlnumElemntos: Integer;
    // ml2Numeros: array[20] of Text;
    // y: Integer;
    // t: Integer;
    begin

        Randomize();
        for i := 1 to ArrayLen(mlNumeros) do begin
            //mlNumeros[i] := Format(Random(ArrayLen(mlNumeros)));
            mlNumeros[i] := Format(i);
        end;
        /*
                xlnumElemntos := CompressArray(mlNumeros);
                Message('la bola que ha salido es %1');
        */
        repeat
            mlNumeros[1] := '';
            i := CompressArray(mlNumeros);
        until CompressArray(mlNumeros) <= 0

    end;

    /// <summary>
    /// UltimLineaF.
    /// </summary>
    /// <param name="pRec">Record "Sales Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimLineaF(pRec: Record "Sales Line"): Integer

    var
        rlRec: Record "Sales Line";

    begin
        //filtros rangos 
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");//claves
        rlRec.SetRange("Document Type", pRec."Document Type");//rangos
        rlRec.SetRange("Document No.", pRec."Document No.");//rangos
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.")
        end;//uso exclusivo para encontrar y solo devuleve uno
    end;

    /// <summary>
    /// ultimo linea de compras
    /// </summary>
    /// <param name="pRec">Record "Purchase Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimLineaF(pRec: Record "Purchase Line"): Integer

    var
        rlRec: Record "Purchase Line";

    begin
        //filtros rangos 
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");//claves
        rlRec.SetRange("Document Type", pRec."Document Type");//rangos
        rlRec.SetRange("Document No.", pRec."Document No.");//rangos
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.")
        end;//uso exclusivo para encontrar y solo devuleve uno 
    end;
    //con los campos distintos,por ejermplo el diario 
    /// <summary>
    /// devuelve el ultimo lin del diario general
    /// </summary>
    /// <param name="pRec">Record "Gen. Journal Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimLineaF(pRec: Record "Gen. Journal Line"): Integer

    var
        rlRec: Record "Gen. Journal Line";

    begin
        //filtros rangos 
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");//claves
        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");//rangos
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");//rangos
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.")
        end;//uso exclusivo para encontrar y solo devuleve uno 
    end;

    /// <summary>
    /// funcionn que se le añadeel parameto booelando
    /// </summary>
    /// <param name="p1">Integer.</param>
    /// <param name="p2">Decimal.</param>
    /// <param name="p3">Text.</param>
    procedure EjemlploSobrecargaF(p1: Integer; p2: Decimal; p3: Text)
    begin
        EjemlploSobrecargaF(p1, p2, p3, false);
    end;

    /// <summary>
    /// funcion del principio original 
    /// </summary>
    /// <param name="p1">Integer.</param>
    /// <param name="p2">Decimal.</param>
    /// <param name="p3">Text.</param>
    /// <param name="pBoolean">Boolean.</param>
    procedure EjemlploSobrecargaF(p1: Integer; p2: Decimal; p3: Text; pBoolean: Boolean)
    var
        xNuevoNumero: Decimal;
    begin
        p1 := 1;
        p2 := 35.5;
        p3 := 'ij';
        //Nueva funcionalidad 
        if pBoolean then begin
            xNuevoNumero := p1 * p2;
        end;
    end;

    /// <summary>
    /// UsoFuncionEjemploF.
    /// </summary>
    procedure UsoFuncionEjemploF()
    begin
        EjemlploSobrecargaF(10, 34.56, 'Hola mundo');
    end;


    local procedure OtrasFuncioneF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        rlSalesInvoiceHeader.TransferFields(rlSalesHeader);
    end;

    /// <summary>
    /// usando listas 
    /// </summary>
    procedure UsodeListasF()
    var
        rlCustLedgerEntry: Record "Cust. Ledger Entry";
        //pglCustLedgEntries: Page "Customer Ledger Entries";
        xlFilterPageBuilder: FilterPageBuilder;
        // xlfechaTrampa: Date;
        //  xDic: Dictionary of [Code[20], Decimal];
        xlMovCtes: Label 'Movimientos de cliente';
        xlClientes: List of [Code[20]];
    //  xlText: TextBuilder;//la que guardara 

    begin
        xlFilterPageBuilder.PageCaption('Selecciona los resgitos a contar');
        xlFilterPageBuilder.AddRecord(xlMovCtes, rlCustLedgerEntry);
        xlFilterPageBuilder.AddField(xlMovCtes, rlCustLedgerEntry."Posting Date");

        if xlFilterPageBuilder.RunModal() then begin
            rlCustLedgerEntry.SetView(xlFilterPageBuilder.GetView(xlMovCtes));
            rlCustLedgerEntry.SetLoadFields("Customer No.");
            if rlCustLedgerEntry.FindSet(false) then begin
                repeat
                    if not xlClientes.Contains(rlCustLedgerEntry."Customer No.") then begin
                        xlClientes.Add(rlCustLedgerEntry."Customer No.");
                    end;
                until rlCustLedgerEntry.Next() = 0;
                Message(' Hay %1 clientes en el filro de %2', xlClientes.Count, rlCustLedgerEntry.GetFilters);
            end else begin
                Message('No hay registros dentro del filtro %1', rlCustLedgerEntry.GetFilters);
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
        //////////hecho por mi 
        // xlfechaTrampa := 20221212D;
        // pglCustLedgEntries.LookupMode(true);
        // rlCustLedgerEntry.SetRange("Posting Date", xlfechaTrampa);
        // if rlCustLedgerEntry.FindSet(false) then begin
        //     repeat
        //         xlClientes.Add(Format(rlCustLedgerEntry."Entry No."));
        //     until rlCustLedgerEntry.Next() = 0;
        // end;
        // Message('contador: %1', xlClientes.Count);
        // if pglCustLedgEntries.RunModal() in [Action::LookupOK] then begin
        //     Message('hola');
        // end;
        //xlLista.
    end;

    //Pantalla de lista de empresas con los registros que hay de Cliente,
    /// <summary>
    /// TotaldeClientesF.
    /// </summary>
    procedure TotaldeClientesF()
    var
    // TempLCustomer: Record Customer temporary;
    // xlClientes: List of [Code[20]];
    begin

    end;




}