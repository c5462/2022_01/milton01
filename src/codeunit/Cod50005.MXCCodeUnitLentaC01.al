/// <summary>
/// la vamos a usar para relizar procesos que tarde mucho
/// </summary>
codeunit 50005 "MXCCodeUnitLentaC01"
{
    Permissions =
        tabledata "Company Information" = R,
        tabledata Customer = RM,
        tabledata "Dimension Set Entry" = R,
        tabledata MXCTablaLogC01 = RIM,
        tabledata "Sales Line" = R;

    trigger OnRun()
    begin
        //siempre por milisegundos
        EjecutarTareaF();

    end;

    //funcion que pasando una cadena y una posicion devuelva 
    /// <summary>
    /// funcion que le paso una cadena y la posicion que quiero que devulva por la izquierda
    /// </summary>
    /// <param name="pCadena">Text.</param>
    /// <param name="pNumCaracteres">Integer.</param>
    /// <returns>Return value of type Tex.</returns>
    procedure LeftF(pCadena: Text; pNumCaracteres: Integer): Text// xSalida: Text
    begin
        // xSalida := CopyStr(pCadena, pNumCaracteres);
        // exit(xSalida);
        if pNumCaracteres > StrLen(pCadena) then begin
            Error('Nº caracteres superior a la long de la cadena');
        end;
        exit(CopyStr(pCadena, 1, pNumCaracteres));
    end;

    /// <summary>
    /// funcion que le paso una cadena y la posicion que quiero que devulva por la derecha
    /// </summary>
    /// <param name="pCadena">Text.</param>
    /// <param name="pNumCaracteres">Integer.</param>
    /// <returns>Return value of type Text.</returns>
    procedure RightF(pCadena: Text; pNumCaracteres: Integer): Text
    begin
        //ABCDEFGH---->3
        exit(CopyStr(pCadena, (StrLen(pCadena) - pNumCaracteres) + 1));
    end;

    /// <summary>
    /// SintaxisF.
    /// </summary>
    procedure SintaxisF()
    var
        rlCustomer: Record Customer;

        xboolean: Boolean;
        xlFecha: Date;
        ventana: Dialog;
        i: Integer;
        x: Integer;
        xlong: Integer;
        xlPos: Integer;
        clSeparador: Label '·', Locked = true;
        xlCampos: List of [Text];
        xlTexto: Text;
    begin
        xlTexto := 'mIlton';
        xlTexto := xlTexto + 'Cabezas';
        i := 9 div 2;
        i := 9 mod 2;
        //Absoluto 
        i := Abs(-9);
        //los valores logicos
        xboolean := not true;
        xboolean := false and true; //falso mientras que los dos den verdadero
        xboolean := false or true;//el que tenga valor verdadero 
        xboolean := false xor true;//cualquiera de los dos pero no los dos.
        //operadores relacionales
        xboolean := 1 < 2;//verdadero
        xboolean := 1 > 2;//flaso
        xboolean := 1 <= 2;//verdadero
        xboolean := 1 >= 2; //False
        xboolean := 1 in [1, 'B', 3, 'Y'];//solo con variables simples
        xboolean := 'a' in ['a' .. 'z'];//el abcedario de a ala z
        //Funciones muy usadas

        Message('Proceso finalizado');
        Message('EL cliente %1 no tiene saldo pendiente de pago ', rlCustomer.Name);
        //no confundir la pisicion occn el tant o porciero 
        Message('Estamos en el año %1%2%1%1', 2, 0);
        Message('proceso finalizado por el usuario');
        Message('proceso finalizado por el usuario : %1', UserId);
        Commit();//suplantando identidad
        Error('');
        xboolean := Confirm('Confirma que desea registrar la factura %1', false, 'FV-1518484'); //preguntar siemppre lo menos traumatico 
        //EL menuuu
        i := StrMenu('Enviar,Facturar,Enviar y Facturar', 3, '¿Como quiere registrar ?');
        case i of
            0:
                Error('Processo cancelado por el usuario');

            1:
                ;//Enviar pedido
            2:
                ;//Facturar
            3:
                ;//Enviar y factiur 

        end;
        //siempre que tenga intervension con el usuario, notificcones , ventanas algo que se muestre al usuario
        if GuiAllowed then begin
            ventana.Open('Procesando bucle #1#########\' + 'Procesando bucle x=#2#####');//si no se abre la ventana no se puede usar ,siempre es un STRing .....\ es un salto linea

        end;
        for i := 1 to 10 do begin
            if GuiAllowed then begin
                ventana.Update(1, i);
                for x := 1 to 10 do begin
                    if GuiAllowed then begin
                        ventana.Update(2, x);

                    end;
                end;
            end;
        end;
        if GuiAllowed then begin
            ventana.Close();

        end;




        //Funcioes de cadenaas 

        xlong := MaxStrLen(xlTexto);//hay que guardarlo en una variable de tipo inte
        xlong := MaxStrLen('ABC');
        xlTexto := CopyStr('ABC', 2, 1);//B
        xlTexto := CopyStr('ABC', 2);//BC
        xlTexto := xlTexto.Substring(2);
        //Devulve la posiocion de un subString ELEFANTE E
        xlPos := StrPos('Elefante', 'e');//3
        xlPos := xlTexto.IndexOf('e');
        xlong := StrLen('ABCD    ');
        xlTexto := UpperCase(xlTexto);//Mayusculas
        xlTexto := xlTexto.ToUpper();
        xlTexto := LowerCase(xlTexto);//Minusculas
        xlTexto := xlTexto.ToLower();
        rlCustomer.Validate("Search Name", rlCustomer.Name.ToLower());
        xlTexto := xlTexto.TrimEnd('\') + '\';
        xlTexto := '1234,123,TECOn servicios, s.l ,234';
        Message(SelectStr(4, xlTexto));//s.l//numero que quiero coger y de la cadena que quiero separado por comas
        xlTexto := '1234,123,TECOn servicios, s.l ,234';
        Message(xlTexto.Split('·').Get(4));//789  //el s
        xlCampos := xlTexto.Split(clSeparador);
        Message(xlCampos.Get(4)); //789
        Message(xlTexto.Split(clSeparador).Get(1));//lo que combierte en una lista

        foreach xlTexto in xlTexto.Split(clSeparador) do begin

        end;
        //Evaluate es lo contrario al format /cualquioer texto me lo combierte a la variable //devuelce un booleano si puedee...
        xlTexto := '21-05-2022';
        //Evaluate(xlFecha,xlTexto);//como no se puede 
        Evaluate(xlFecha, ConvertStr(xlTexto, '-', '/'));
        Evaluate(xlFecha, xlTexto.Replace('-', '/').Replace(',', '.'));



        xlTexto := 'Elefante ';
        Message(DelChr(xlTexto, '<=>', 'e'));//<=> toods los sitios //'<' AL principo 

        xlPos := Power(3, 2);//potencias

        xlTexto := CompanyName();//devueelve el nombre de la empresa
        Today;///
        WorkDate();//fecha de trabajo
        Time;
        ///nose sabe para que sirve los date
    //una variable tipo blob(Contenedor FIle o Blob)


    end;

    /// <summary>
    /// StreamsF.
    /// </summary>
    procedure StreamsF()
    var
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary; //los temporales no tienen validate , sin delete all
        xlInStr: InStream;
        xlOutStr: OutStream;

        //simpre que haya que usar delete all, cuidado con  la tabla temporal 
        xlinea: Text;
        xlNombreFichero: Text;

    begin
        TempLMXCConfiguracionC01.MiBlob.CreateOutStream(xlOutStr);
        xlOutStr.WriteText('MXC');
        TempLMXCConfiguracionC01.MiBlob.CreateInStream(xlInStr);
        xlInStr.ReadText(xlinea);
        xlNombreFichero := 'Pedido.csv';
        // DownloadFromStream()
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;//cualquier trafico que se hace se hace con el instrema

    end;

    local procedure ClienteF()
    var
        rCustomer: Record Customer;
    begin
        rCustomer.get('10000');
    end;

    local procedure EjecutarTareaF()
    var
        xEjecutar: Boolean;
    begin

        if GuiAllowed then begin
            xEjecutar := Confirm('¿Desea ejecutar esta tarea que tarde mucho?')
        end;
        if xEjecutar then begin
            InsertarLogF('Tarea Iniciada');
            //no usar casi nunca
            Commit();//pude que  haya errores 
            Sleep(5000);
            InsertarLogF('Tarea Finalizada');

        end;

    end;

    local procedure funcionejemploF()
    var
        rlCustomer: Record Customer;
        rlCustomer2: Record Customer;
        rlSalesLine: Record "Sales Line";
        rlSalesLine2: Record "Sales Line";
    begin
        rlCustomer.SetFilter(Name, '%1|%2', '*A*', '*B*');
        if rlCustomer.FindSet() then begin
            repeat
                rlCustomer2 := rlCustomer;
                rlCustomer2.Find();
                rlCustomer2.Validate(Name, DelChr(rlCustomer2.Name, '<=>', 'AB'));
                rlCustomer2.Modify(true);
            until rlCustomer.Next() = 0;
        end;



        if rlSalesLine.FindSet() then begin
            repeat
                rlSalesLine2 := rlSalesLine;
                rlSalesLine2.Find();

                rlSalesLine2.get(rlSalesLine."Document Type", rlSalesLine."Document No.", rlSalesLine."Line No.");

            // Modificaciones
            until rlSalesLine.Next() = 0;
        end;
    end;


    local procedure getcompanyinfoF()
    var
        rlcompany: Record "Company Information";
        rsalesLine: Record "Sales Line";
    begin
        rlcompany.get();
        //todos los find respetan los filtos
        if rsalesLine.FindSet() then begin
            repeat

            until rsalesLine.Next() = 0;
        end;
    end;

    //Dimension Set Entry 

    local procedure getDimencio()
    var
        rldime: Record "Dimension Set Entry";
    begin
        //"Dimension Code", "Dimension Value Code", "Dimension Set ID"

        rldime.get('GRUPONEGOCIO', 'INICIO', 7);
        //otra opcion 
        //"Dimension Set ID", "Dimension Code"
        rldime.Get(7, 'GRUPONEGOCIO');



    end;

    local procedure getLineaF()
    var
        rsales: Record "Sales Line";
    begin
        rsales.get(rsales."Document Type"::Order, 101005, 10000);
    end;

    local procedure InsertarLogF(pMensaje: Text)
    var
        rlLog: Record MXCTablaLogC01;
    begin
        rlLog.Init();
        //el true por se un segistro de tabla
        rlLog.Insert(true);
        rlLog.Validate(Mensaje, pMensaje);
        rlLog.Modify(true);

    end;

}