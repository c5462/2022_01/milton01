/// <summary>
/// Codeunit "MXCActualizarAppC01" (ID 50106). Se ejecutará cuando actualicemos la App
/// </summary>
codeunit 50006 MXCActualizarAppC01
{
    Subtype = Upgrade;

    trigger OnCheckPreconditionsPerCompany()
    begin

    end;

    trigger OnCheckPreconditionsPerDatabase()
    begin

    end;

    // ------- Estos son los que más se utilizan, ya que dentro podrías hacer el chequeo de precondiciones -------//
    // ------- y las verificaciones posteriores a la actualización (en Tecon se hace con estas dos) --------------//

    trigger OnUpgradePerCompany()
    begin

    end;

    trigger OnUpgradePerDatabase()
    begin

    end;
    // -----------------------------------------------------------------------------------------------------------//

    trigger OnValidateUpgradePerCompany()
    begin

    end;

    trigger OnValidateUpgradePerDatabase()
    begin

    end;

}
