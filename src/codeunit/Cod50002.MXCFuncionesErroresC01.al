/// <summary>
/// Esta codeUnit es para capturar errores
/// </summary>
/////////////////LOS ERRORES PARAN LA EJECUCION DEL CODEUNIT////////
codeunit 50002 "MXCFuncionesErroresC01"
{
    //la mayoria d las codes llevan singleInstance
    SingleInstance = true;
    trigger OnRun()
    var
        //varable local
        rlCustomer: Record Customer;
        clCodtCte: Label 'AAA', Locked = true, Comment = 'un comentariio para el proo';
    begin

        //rlCustomer.get(clCodtCte);
        //Message('Datos del cliente : %1', rlCustomer);
        if not rlCustomer.get(clCodtCte) then begin
            rlCustomer.Init();
            //obligatorioooooooooooooooo/insersion de datos/ valores de la clave principal  Rec.insert(). 
            rlCustomer.Validate("No.", 'BBB');
            //cuando una tabla no es temporal el validate o el inset es true o cuando queramos que se 
            rlCustomer.Insert(true);
            rlCustomer.Validate(Name, 'Prooveedor prueba');
            rlCustomer.Validate(Address, 'Mi calle');
            rlCustomer."VAT Registration No." := 'XXXXXX';//Para que no modifique si el cird esta en otro proveedor 
            rlCustomer.Validate("Payment Method Code", 'MXC');
            rlCustomer.Modify(true);

        end;
        Message('Datos del cliente : %1', rlCustomer);

    end;
    //funcion normal que 
    /// <summary>
    /// funcion try que no tendra una modificacon de la bbdd
    /// </summary>
    /// <param name="pCodProveedor">Code[20].</param>
    //[TryFunction]
    procedure FuciontoCaptureErrorF(pCodProveedor: Code[20]): Boolean
    var
        rlVendor: Record Vendor;
    begin
        // rlVendor.get('BBB');
        //insertar registros en una tabla
        if not rlVendor.get(pCodProveedor) then begin
            rlVendor.Init();
            //obligatorioooooooooooooooo/insersion de datos/ valores de la clave principal  Rec.insert(). 
            rlVendor.Validate("No.", pCodProveedor);
            //cuando una tabla no es temporal el validate o el inset es true o cuando queramos que se 
            rlVendor.Insert(true);
            if AsignarDatosProveedorF(rlVendor) then begin
                rlVendor.Modify(true)
            end else begin
                Message('Error en asignar datos (%1)', GetLastErrorText());
            end;
            exit(true);
        end;
        Message('YA existe el provedor : %1', pCodProveedor);
    end;
    //es 
    [TryFunction]
    //esto es un procedimiento 
    local procedure AsignarDatosProveedorF(var rlVendor: Record Vendor)
    begin
        rlVendor.Validate(Name, 'Prooveedor prueba');
        rlVendor.Validate(Address, 'Mi calle');
        rlVendor."VAT Registration No." := 'XXXXXX';//Para que no modifique si el cird esta en otro proveedor 
        rlVendor.Validate("Payment Method Code", 'MXC');
    end;
}