/// <summary>
/// Codeunit MXCFuncionesC01 (ID 50100).
/// </summary>
codeunit 50000 "MXCFuncionesC01"
{
    Subtype = Install;// Se ejecutará automáticamente cuando instalemos la App
    SingleInstance = true;
    //
    trigger OnInstallAppPerCompany()// Se ejecuta tantas veces como empresas tenga
    begin

    end;

    trigger OnInstallAppPerDatabase()// Se ejecuta una vez por cada BBDD. Para tablas comunes a todas las empresas.
    begin

    end;

}
