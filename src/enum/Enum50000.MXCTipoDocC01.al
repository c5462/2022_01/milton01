/// <summary>
/// Un enum para elegir el tipo de documento que queremos imprimir
/// </summary>
enum 50000 "MXCTipoDocC01"
{
    //Extensible = false;

    value(0; "")
    {
    }
    value(10; Producto)
    {
    }
    value(20; Cuenta)
    {
    }
    value(30; Activo)
    {
    }
}
