/// <summary>
/// Enum MXCFuel TypeC01 (ID 80105).
/// </summary>
enum 50005 MXCFuelTypeC01
{
    Extensible = true;
    value(0; Petrol)
    {
        Caption = 'Petrol';
    }
    value(1; Diesel)
    {
        Caption = 'Diesel';
    }
    value(2; Electric)
    {
        Caption = 'Electric';
    }
}