/// <summary>
/// Enum MXCColoresC01 (ID 80102).
/// </summary>
enum 50002 "MXCColoresC01"
{
    Extensible = false;

    value(0; Blanco)
    {
        Caption = 'blanco';
    }
    value(10; Rojo)
    {
        Caption = 'rojo';
    }
    value(20; Gris)
    {
        Caption = 'gris';
    }
    value(30; Negro)
    {
        Caption = 'negro';
    }
}
