/// <summary>
/// Enum MXCTipoTablaC01 (ID 80101).
/// </summary>
enum 50001 "MXCTipoTablaC01"
{

    //no hay que poner lo de extensible
    // Extensible = false;

    value(0; Cliente)
    {
        Caption = 'Tabla de Cliente';
    }
    value(10; Proveedor)
    {
        Caption = 'Proveedor';
    }
    value(20; Empleado)
    {
        Caption = 'Empleado';
    }
    value(30; Recurso)
    {
        Caption = 'Recurso';
    }
    value(40; Contacto)
    {
        Caption = 'Contacto';
    }
}
