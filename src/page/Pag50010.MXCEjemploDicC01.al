/// <summary>
/// Page MXCEjemploDicC01 (ID 80110).
/// </summary>
page 50010 "MXCEjemploDicC01"
{
    ApplicationArea = All;
    Caption = 'Ejemplos Diccionarios';
    PageType = List;
    SourceTable = Integer;//una tabla para tratar lis 
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(num; Rec.Number)
                {
                    Caption = 'Numero';
                    ApplicationArea = All;
                }
                field(Campo01; valorCeldaF(Rec.Number, 1))
                {
                    Caption = 'Valor Celda 01';
                    ApplicationArea = All;
                }
                field(Campo02; valorCeldaF(Rec.Number, 2))
                {
                    Caption = 'Valor Celda 02';
                    ApplicationArea = All;
                }
                field(Campo03; valorCeldaF(Rec.Number, 3))
                {
                    Caption = 'Valor Celda 03';
                    ApplicationArea = All;
                }
                field(Campo04; valorCeldaF(Rec.Number, 4))
                {
                    Caption = 'Valor Celda 04';
                    ApplicationArea = All;
                }
                field(Campo05; valorCeldaF(Rec.Number, 5))
                {
                    Caption = 'Valor Celda 05';
                    ApplicationArea = All;
                }
                field(Campo06; valorCeldaF(Rec.Number, 6))
                {
                    Caption = 'Valor Celda 06';
                    ApplicationArea = All;
                }
                field(Campo07; valorCeldaF(Rec.Number, 7))
                {
                    Caption = 'Valor Celda 07';
                    ApplicationArea = All;
                }

            }
        }
    }

    trigger OnOpenPage()
    var
        xlColumna: Integer;
        xlFila: Integer;
        xlnumeros: List of [Decimal];

    begin
        for xlFila := 1 to 10 do begin
            Clear(xlnumeros);
            for xlColumna := 1 to 8 do begin
                xlnumeros.Add(Random(5555));
            end;
            xDicDecimal.Add(xlFila, xlnumeros);
        end;
        Rec.SetRange(Number, 1, 10);
        ejemplooNotificacionF();
    end;
    ///vamos a creat rel diccionarion de 
    var
        xDicDecimal: Dictionary of [Integer, List of [Decimal]];

    local procedure valorCeldaF(pFila: Integer; pColumna: Integer): Decimal
    begin
        exit(xDicDecimal.Get(pFila).Get(pColumna));
    end;

    local procedure ejemplooNotificacionF()
    var
        // xlNewNotificacion: Notification;
        xlNotificacion: Notification;
        newLabel: Label 'Carga %1 finalizada';
        asnewLabel: Label 'AbrirLaListaCtesF';
        i: Integer;
    begin
        for i := 1 to 3 do begin
            xlNotificacion.Message(StrSubstNo(newLabel, i));

            xlNotificacion.Send();
        end;
        xlNotificacion.Id(CreateGuid());
        xlNotificacion.Message('El cliente no tiene vacuna');
        //abrir la lista de clientes por ejemplo 
        xlNotificacion.AddAction('Abrir lista de clientes', Codeunit::"Sales-Post", asnewLabel);
        //enviar un dato 
        xlNotificacion.SetData('Codigo Cliente', '10000');
        xlNotificacion.Send();


    end;
}
