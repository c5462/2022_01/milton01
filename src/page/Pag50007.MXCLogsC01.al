/// <summary>
/// pagina de la tabla log
/// </summary>
page 50007 "MXCLogsC01"
{
    ApplicationArea = All;
    Caption = 'LogsC01';
    PageType = List;
    SourceTable = MXCTablaLogC01;
    UsageCategory = Lists;

    //ordenar siempre por campos 
    SourceTableView = sorting(SystemCreatedBy) order(descending);
    InsertAllowed = false;
    ModifyAllowed = false;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id field.';
                    ApplicationArea = All;
                }
                field(Mensaje; Rec.Mensaje)
                {
                    ToolTip = 'Specifies the value of the Mensaje field.';
                    ApplicationArea = All;
                }
            }
        }
    }


}
