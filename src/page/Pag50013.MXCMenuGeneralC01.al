
/// <summary>
/// Page MXCMenuGeneralC01 (ID 80113).
/// </summary>
page 50013 MXCMenuGeneralC01
{
    ApplicationArea = All;
    Caption = 'Menú App utilidades';
    PageType = List;
    UsageCategory = Administration;
    ShowFilter = false;
    RefreshOnActivate = true;
    PromotedActionCategories = 'Menu, , ,Listas,Informes,Tareas,Configuración';
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;

    layout
    {
        area(Content)
        {
            usercontrol(NombreMenu; "Microsoft.Dynamics.Nav.Client.WebPageViewer")
            {
                ApplicationArea = all;
                trigger ControlAddInReady(callbackUrl: Text)
                begin
                    HtmlNombreMenu(CurrPage.NombreMenu, CurrPage.Caption);
                end;
            }
            usercontrol(logo; MXCLogoMiniC01)
            {
                ApplicationArea = all;
            }
        }
    }
    actions
    {
        area(Processing)
        {


            group(GrupoLists)
            {
                Caption = 'GrpMXC';
                action(Acc61)
                {
                    Caption = 'Car models';
                    Image = CalculateRemainingUsage;
                    Promoted = true;
                    PromotedCategory = Category4;
                    ApplicationArea = All;
                    RunPageMode = View;
                    RunObject = page MXCApiTestC01;
                }
                action(Acc6)
                {
                    Caption = 'MXCAPIsITEMC01';
                    Image = CalculateRemainingUsage;
                    Promoted = true;
                    PromotedCategory = Category4;
                    ApplicationArea = All;
                    RunPageMode = View;
                    RunObject = page MXCAPIsITEMC01;
                }


            }
            group(GrpReport)
            {
                Caption = 'Informes';
                action("Report")
                {
                    Caption = 'Listado productos';
                    Image = Report;
                    Promoted = true;
                    PromotedCategory = Category5;
                    ApplicationArea = All;
                    RunPageMode = View;
                    RunObject = report MXCListadoProductosC01;
                }

#if IVAprorrataROT
                action(Acc92)
                {
                    Caption = 'Porcentajes no deducibles';
                    Image = Production;
                    Promoted = true;
                    PromotedCategory = Category9;
                    ApplicationArea = All;
                    RunPageMode = View;
                    RunObject = page ROTPorcentajesNoDeduciblesGEN;
                }
#endif
            }

            group(GrupoTask)
            {
                Caption = 'Lista varios';
                action(Acc71)
                {
                    Caption = 'Varios';
                    Image = Production;
                    Promoted = true;
                    PromotedCategory = Category6;
                    ApplicationArea = All;
                    RunPageMode = View;
                    RunObject = page MXCListaVariosC01;
                }

            }
            group(GrpSetup)
            {
                Caption = 'Configuración';
                action(Setup)
                {
                    Caption = 'Reg. solicitud compras (Pedidos)';
                    Image = Setup;
                    Promoted = true;
                    PromotedCategory = Category7;
                    ApplicationArea = All;
                    RunPageMode = View;
                    RunObject = page MXCConfiguracionC01;
                }

            }
        }
    }

    /// <summary>
    /// Enseña el Nombre del Menu.
    /// </summary>
    /// <param name="pControl">ControlAddIn "Microsoft.Dynamics.Nav.Client.WebPageViewer".</param>
    /// <param name="pTitulo">Text.</param>
    procedure HtmlNombreMenu(pControl: ControlAddIn "Microsoft.Dynamics.Nav.Client.WebPageViewer"; pTitulo: Text)
    var
        clHtml: Label '<html> <body style="text-align:center;"><h2 style="background-color:powderblue;text-align:center;font-size:200%;font-family:Garamond;">%1</h2></body></html>';
    begin
        pControl.SetContent(StrSubstNo(clHtml, pTitulo));
    end;
}
