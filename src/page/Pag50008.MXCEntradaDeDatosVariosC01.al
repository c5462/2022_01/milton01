/// <summary>
/// pagina comodin
///
/// </summary>
page 50008 "MXCEntradaDeDatosVariosC01"
{
    Caption = 'Entrada de datos varios';
    PageType = StandardDialog;


    layout
    {
        area(content)
        {
            field(Texto01; mTextos[1])
            {
                ApplicationArea = all;
                Visible = xTxtVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 1];
            }
            //corregido 
            field(Texto02; mTextos[2])
            {
                ApplicationArea = all;
                Visible = xTxtVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 2];
            }
            field(Texto03; mTextos[3])
            {
                ApplicationArea = all;
                Visible = xTxtVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 3];
            }
            field(Texto04; mTextos[4])
            {
                ApplicationArea = all;
                Visible = xTxtVisible04;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 4];
            }
            //campos tipo boolean
            field(Bolean01; mBoolean[1])
            {
                Visible = xbool01;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 1];
                ApplicationArea = All;
            }
            field(Bolean02; mBoolean[2])
            {
                Visible = xbool02;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 2];
                ApplicationArea = All;
            }
            field(Bolean03; mBoolean[3])
            {
                Visible = xbool03;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 3];
                ApplicationArea = All;
            }
            //campos tipo numerico
            field(Numeric01; mNumeric[1])
            {
                Visible = xNumer01;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 1];
                ApplicationArea = All;
            }
            field(Numeric02; mNumeric[2])
            {
                Visible = xNumer02;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 2];
                ApplicationArea = All;
            }
            field(Numeric03; mNumeric[3])
            {
                Visible = xNumer03;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 3];
                ApplicationArea = All;
            }
            //campos tipo Fecha
            field(Fecha01; mFecha[1])
            {
                Visible = xFechBol01;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 1];
                ApplicationArea = All;
            }
            field(Fecha02; mFecha[2])
            {
                Visible = xFechBol02;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 2];
                ApplicationArea = All;
            }
            field(Fecha03; mFecha[3])
            {
                Visible = xFechBol03;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 3];
                ApplicationArea = All;
            }
            //Confirmacion de Contraeña
            field(Pass01; mpass[1])
            {
                Visible = xPass01;
                CaptionClass = mCaptionClass[xTipoDato::Pass, 1];
                ApplicationArea = All;
                ExtendedDatatype = Masked;
            }
            field(Pass02; mpass[2])
            {
                Visible = xPass02;
                CaptionClass = mCaptionClass[xTipoDato::Pass, 2];
                ApplicationArea = All;
                ExtendedDatatype = Masked;
            }
            field(Pass03; mpass[3])
            {
                Visible = xPass03;
                CaptionClass = mCaptionClass[xTipoDato::Pass, 3];
                ApplicationArea = All;
                ExtendedDatatype = Masked;
            }


        }
    }
    var
        mBoolean: Array[10] of Boolean;
        xbool01: Boolean;
        xbool02: Boolean;
        xbool03: Boolean;
        xFechBol01: Boolean;
        xFechBol02: Boolean;
        xFechBol03: Boolean;
        xNumer01: Boolean;
        xNumer02: Boolean;
        xNumer03: Boolean;
        xPass01: Boolean;
        xPass02: Boolean;
        xPass03: Boolean;
        xTxtVisible01: Boolean;
        xTxtVisible02: Boolean;
        xTxtVisible03: Boolean;
        xTxtVisible04: Boolean;
        mFecha: Array[10] of Date;
        //mCaptionClass: Array[10] of Text;
        /// <summary>
        /// campoF de xpuntero
        /// </summary>
        mNumeric: Array[10] of Decimal;
        xpunteros: array[2, 5] of Integer;
        //xpunteros: array[5] of Integer;
        xTipoPuntero: Option Nulo,Pasado,Leido;
        //var tipo option solo para esta pagina 
        xTipoDato: Option Nulo,Texto,Booleano,Numerico,Fecha,Pass;
        mCaptionClass: array[5, 10] of Text;
        mpass: Array[10] of Text;
        mTextos: array[10] of Text;
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text)
    begin
        xpunteros[xTipoPuntero::Pasado, xTipoDato::Texto] += 1;
        mCaptionClass[xTipoDato::Texto, xpunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pCaption;
        mTextos[xpunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pValorInicial;
        xTxtVisible01 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 1;
        xTxtVisible02 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 2;
        xTxtVisible03 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 3;
        xTxtVisible04 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 4;

    end;

    /// <summary>
    /// funcion para la contraseña 
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    /// <param name="pPass">Boolean.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text; pPass: Boolean)
    begin
        if pPass then begin
            xpunteros[xTipoPuntero::Pasado, xTipoDato::Pass] += 1;
            mCaptionClass[xTipoDato::Pass, xpunteros[xTipoPuntero::Pasado, xTipoDato::Pass]] := pCaption;
            mTextos[xpunteros[xTipoPuntero::Pasado, xTipoDato::Pass]] := pValorInicial;
            xPass01 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Pass] >= 1;
            xPass02 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Pass] >= 2;
            xPass03 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Pass] >= 3;

        end else begin
            CampoF(pCaption, pValorInicial);
        end;
    end;
    //EN boolean
    /// <summary>
    /// Hace que se carge un boleando
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Boolean.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Boolean)
    begin
        /// <summary>
        /// Hace que se muestre un campo tipo texto conel caption y valor pasados
        /// </summary>
        xpunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] += 1;
        //mboolCaptionClass[xPunteroDeBoollPasados] := pCaption;
        mCaptionClass[xTipoDato::Booleano, xpunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pCaption;
        //mTextos[xPunteroDeBoollPasados] := Format(pValorInicial);
        mBoolean[xpunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pValorInicial;
        xbool01 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 1;
        xbool02 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 2;
        xbool03 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 3;
    end;

    /// <summary>
    /// Tipo Decimal
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Decimal.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Decimal)
    begin
        /// <summary>
        /// Hace que se muestre un campo tipo texto conel caption y valor pasados
        /// </summary>
        xpunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] += 1;
        //mboolCaptionClass[xPunteroDeBoollPasados] := pCaption;
        mCaptionClass[xTipoDato::Numerico, xpunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pCaption;
        //mTextos[xPunteroDeBoollPasados] := Format(pValorInicial);
        mNumeric[xpunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pValorInicial;
        xNumer01 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 1;
        xNumer02 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 2;
        xNumer03 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 3;
    end;

    /// <summary>
    /// Funcion para la fecha 
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Date.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Date)
    begin
        /// <summary>
        /// Hace que se muestre un campo tipo texto conel caption y valor pasados
        /// </summary>
        xpunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] += 1;
        //mboolCaptionClass[xPunteroDeBoollPasados] := pCaption;
        mCaptionClass[xTipoDato::Fecha, xpunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pCaption;
        //mTextos[xPunteroDeBoollPasados] := Format(pValorInicial);
        mFecha[xpunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pValorInicial;
        xFechBol01 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 1;
        xFechBol02 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 2;
        xFechBol03 := xpunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 3;
    end;




    /// <summary>
    /// Devuelve el texto introducido por el usuario
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(): Text
    begin
        xpunteros[xTipoPuntero::Leido, xTipoDato::Texto] += 1;
        exit(mTextos[xpunteros[xTipoPuntero::Leido, xTipoDato::Texto]]);
    end;

    /// <summary>
    /// Funcion para devolcer la Contraseña
    /// </summary>
    /// <param name="pSalida">VAR Text.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text): Text
    begin
        xpunteros[xTipoPuntero::Leido, xTipoDato::Pass] += 1;
        pSalida := mpass[xpunteros[xTipoPuntero::Leido, xTipoDato::Pass]];
        exit(pSalida);
    end;


    /// <summary>
    /// Decimal
    /// 
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CampoF(var pSalida: Boolean): Boolean
    begin
        xpunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        pSalida := mBoolean[xpunteros[xTipoPuntero::Leido, xTipoDato::Booleano]];
        exit(pSalida);
    end;
    //tipo decimal
    /// <summary>
    /// Decimal
    /// </summary>
    /// <param name="pSalida">VAR Decimal.</param>
    /// <returns>Return value of type Decimal.</returns>
    procedure CampoF(var pSalida: Decimal): Decimal
    begin
        xpunteros[xTipoPuntero::Leido, xTipoDato::Numerico] += 1;
        pSalida := mNumeric[xpunteros[xTipoPuntero::Leido, xTipoDato::Numerico]];
        exit(pSalida);
    end;

    /// <summary>
    /// Fecha
    /// </summary>
    /// <param name="pSalida">VAR Date.</param>
    /// <returns>Return value of type Date.</returns>
    procedure CampoF(var pSalida: Date): Date
    begin
        xpunteros[xTipoPuntero::Leido, xTipoDato::Fecha] += 1;
        pSalida := mFecha[xpunteros[xTipoPuntero::Leido, xTipoDato::Fecha]];
        exit(pSalida);
    end;

    ///////Con los puntero///////

    /// <summary>
    /// Devuelve el texto introducido por el usuario del puntero pasado por un puntero
    /// </summary>
    /// <param name="pPuntero">Integer.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(pPuntero: Integer): Text
    begin


        exit(mTextos[pPuntero]);
    end;

    /// <summary>
    /// Funcion para devolcer la Contraseña por el usuario del puntero pasado por un puntero
    /// </summary>
    /// <param name="pSalida">VAR Text.</param>
    /// <param name="pPuntero">Integer.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text; pPuntero: Integer): Text
    begin

        pSalida := mpass[pPuntero];
        exit(pSalida);
    end;


    /// <summary>
    /// Decimal por el usuario del puntero pasado por un puntero
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <param name="pPuntero">Integer.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CampoF(var pSalida: Boolean; pPuntero: Integer): Boolean
    begin

        pSalida := mBoolean[pPuntero];
        exit(pSalida);
    end;
    //tipo decimal
    /// <summary>
    /// Decimal por el usuario del puntero pasado por un puntero
    /// </summary>
    /// <param name="pSalida">VAR Decimal.</param>
    /// <param name="pPuntero">Integer.</param>
    /// <returns>Return value of type Decimal.</returns>
    procedure CampoF(var pSalida: Decimal; pPuntero: Integer): Decimal
    begin

        pSalida := mNumeric[pPuntero];
        exit(pSalida);
    end;

    /// <summary>
    /// Fecha por el usuario del puntero pasado por un puntero
    /// </summary>
    /// <param name="pSalida">VAR Date.</param>
    /// <param name="pPuntero">Integer.</param>
    /// <returns>Return value of type Date.</returns>
    procedure CampoF(var pSalida: Date; pPuntero: Integer): Date
    begin

        pSalida := mFecha[pPuntero];
        exit(pSalida);
    end;
}

