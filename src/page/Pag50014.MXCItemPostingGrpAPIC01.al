
page 50014 MXCItemPostingGrpAPIC01
{
    APIGroup = 'demo';
    APIPublisher = 'bcMxc';
    APIVersion = 'v1.0';
    ApplicationArea = All;
    Caption = 'apIsITEM';
    DelayedInsert = true;
    EntityName = 'itempostGrpMXC';
    EntitySetName = 'itempostGrpMXCs';
    ODataKeyFields = SystemId;
    PageType = API;
    SourceTable = "Inventory Posting Group";

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(systemId; Rec.SystemId)
                {
                }
                field("code"; Rec."Code")
                {
                }
                field(description; Rec.Description)
                {
                }

            }
        }
    }
}
