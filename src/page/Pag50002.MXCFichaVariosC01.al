/// <summary>
/// esto creo que etaba antes ehh
/// </summary>
page 50002 "MXCFichaVariosC01"
{
    Caption = 'Ficha registros varios';
    PageType = Card;
    SourceTable = MXCVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha de tabla varios';
    AboutText = 'Se usará para editar registros de una tabla donde tendremos tipos varios de registros';
    layout
    {
        area(content)
        {
            group(General)
            {
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Specifies the value of the Tipo field.';
                    ApplicationArea = All;
                    AboutTitle = 'Tipo de registro';
                    AboutText = 'Indicará si la el registro es de Vacunas...... y otros';
                }
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(PeridoSegundaVacunacion; Rec.PeridoSegundaVacunacion)
                {
                    ToolTip = 'Specifies the value of the segunda vacunacion field.';
                    ApplicationArea = All;
                }

            }
            group(Informacion)
            {
                Editable = false;
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
