/// <summary>
/// Page MXCApiTestC01 (ID 80112).
/// </summary>
page 50012 MXCApiTestC01
{
    PageType = API;
    Caption = 'Primera API page';
    APIPublisher = 'bcMxc';
    APIGroup = 'demo';
    APIVersion = 'v1.0';
    EntityName = 'carModel';
    EntitySetName = 'carModels';
    SourceTable = MXCCarModelC01;
    DelayedInsert = true;
    ODataKeyFields = SystemId;

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {
                field(systemId; Rec.SystemId)
                {
                }

                field(name; Rec.Name)
                {
                }
                field(description; Rec.Description)
                {
                }
                field(brandId; Rec."Brand Id")
                {
                }
                field(power; Rec.Power)
                {
                }
                field(fuelType; Rec."Fuel Type")
                {
                }
                field(systemModifiedBy; Rec.SystemModifiedBy)
                {
                }
            }
        }
    }
}