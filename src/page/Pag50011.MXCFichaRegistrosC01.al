page 50011 "MXCFichaRegistrosC01"
{
    ApplicationArea = All;
    Caption = 'FichaRegistrosC01';
    PageType = List;
    SourceTable = MXCRegistrosTotalesC01;
    UsageCategory = Lists;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(IDEmpre; Rec.NombreEmpesaF())
                {
                    ToolTip = 'Specifies the value of the IDEmpre field.';
                    ApplicationArea = All;

                }
                field(Clientes; Rec.Clientes)
                {
                    ToolTip = 'Specifies the value of the Clientes field.';
                    ApplicationArea = All;

                }
                field(Proveedores; Rec.Proveedores)
                {
                    ToolTip = 'Specifies the value of the Proveedores field.';
                    ApplicationArea = All;
                }
                field(Productos; Rec.Productos)
                {
                    ToolTip = 'Specifies the value of the Productos field.';
                    ApplicationArea = All;
                }
            }
        }


    }




}
