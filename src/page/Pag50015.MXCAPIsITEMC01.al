page 50015 MXCAPIsITEMC01
{
    APIGroup = 'apiGroup';
    APIPublisher = 'publisherName';
    APIVersion = 'v1.0';
    ApplicationArea = All;
    Caption = 'apIsITEM';
    DelayedInsert = true;
    EntityName = 'bcGrup';
    EntitySetName = 'item01';
    PageType = API;
    SourceTable = Item;
    ODataKeyFields = SystemId;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(systemId; Rec.SystemId)
                {

                }
                field(no; Rec."No.")
                {
                }
                field(description; Rec.Description)
                {
                }
                field(inventoryPostingGroup; Rec."Inventory Posting Group")
                {
                }
            }
        }
    }
}
