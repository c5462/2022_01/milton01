/// <summary>
/// lista del plan
/// </summary>
page 50006 "MXCSubPageLinPlanVacunacioC01"
{
    Caption = 'SubPageLinPlanVacunacioC01';
    PageType = ListPart;
    SourceTable = "MXCLinPlanVacunacionC01";
    AutoSplitKey = true;
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codt; Rec.CodLineas)
                {
                    ToolTip = 'Specifies the value of the Codt field.';
                    ApplicationArea = All;
                    Visible = false;
                    Editable = false;
                }
                field(FechaVacunacion; Rec.FechaVacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha de la Vacunacion field.';
                    ApplicationArea = All;


                }
                field(NumLinea; Rec.NumLinea)
                {
                    Visible = false;
                    Editable = false;
                    ToolTip = 'Specifies the value of the NumLinea field.';
                    ApplicationArea = All;
                }
                field(ClientetoVacuna; Rec.ClientetoVacuna)
                {
                    ToolTip = 'Specifies the value of the ClientetoVacuna field.';
                    ApplicationArea = All;
                    // trigger OnValidate()
                    // //variables locales 
                    // var
                    //     rlMXCPLanVacunacionMXCC01: Record MXCPllanVacunacionC01;
                    // //rlFecaa: Record MXCPllanVacunacionC01;
                    // begin
                    //     if rlMXCPLanVacunacionMXCC01.Get(Rec.CodLineas) then begin
                    //         Rec.Validate(FechaVacunacion, rlMXCPLanVacunacionMXCC01.FechaIniPlanificacion);
                    //     end;

                    // end;
                }
                field(NombreCliente; Rec.NombreClientetoVacunaF())

                {
                    Caption = 'Noombre Cliente';
                    ApplicationArea = All;
                }
                field(Fecha2Vacunacion; Rec.Fecha2Vacunacion)
                {
                    ToolTip = 'Specifies the value of the Fecha 2º Vacunación field.';
                    ApplicationArea = All;
                }
                field(TiposOtros; Rec.CodOtros)
                {
                    ToolTip = 'Specifies the value of the Tipo otros field.';
                    ApplicationArea = All;
                }
                field("Tipo vacuna"; Rec.CodVacuna)
                {
                    ToolTip = 'Specifies the value of the Tipo vacuna field.';
                    ApplicationArea = All;
                }
                field(DescripcionVacuna; Rec.DesVariosF(eMXCTipo::Vacuna, Rec.CodVacuna))
                {
                    ApplicationArea = All;
                }
                field(DescripcionOtros; Rec.DesVariosF(eMXCTipo::Otros, Rec.CodOtros))
                {
                    ApplicationArea = All;
                }

            }
        }
    }
    var
        //  rtipos: Record MXCVariosC01;
        eMXCTipo: Enum MXCTipoC01;
    // //unicamente por pantalla , asigna a ese valor que haga un trigger 
    // trigger OnNewRecord()
    // begin

    //     if xRec.FechaVacunacion <> Rec.FechaVacunacion then begin
    //         //cuando estamos haciendo asignaciones nunca := (variables), !!mejor usar Validate a valores de Campo!!!
    //         //asignacion a variables :=  Rec.CodTabla := '';    
    //         Rec.Validate(Rec.FechaVacunacion);
    //     end;
    // end;


    // trigger OnNewRecord(BelowxRec: Boolean)
    // //variables locales 
    // var
    //     rlMXCPLanVacunacionMXCC01: Record MXCPllanVacunacionC01;
    // //rlFecaa: Record MXCPllanVacunacionC01;
    // begin
    //     if rlMXCPLanVacunacionMXCC01.Get(Rec.CodLineas) then begin
    //         Rec.Validate(FechaVacunacion, rlMXCPLanVacunacionMXCC01.FechaIniPlanificacion);
    //     end;

    // end;
    //



}
