/// <summary>
/// es una lista de la tabla PLanVacunacion
/// </summary>
page 50005 "MXCListaPLanVacunacionC01"
{
    ApplicationArea = All;
    Caption = 'Lista Cabecera PLan VacunacionC01';
    PageType = List;
    SourceTable = MXCPllanVacunacionC01;
    UsageCategory = Lists;
    AdditionalSearchTerms = 'Curso 01';
    CardPageId = MXCFichaPLanVacunacionC01;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    Editable = false;

    /* ///  ApplicationArea = All;
     Caption = 'Lista tabla Varios';
     AdditionalSearchTerms = 'Curso 01';
     PageType = List;
     SourceTable = MXCVariosC01;
     UsageCategory = Lists;
     InsertAllowed = false;
     ModifyAllowed = false;
     DeleteAllowed = false;
     Editable = false;
     CardPageId = MXCFichaVariosC01;
 */
    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codt; Rec.CodCabeceras)
                {
                    ToolTip = 'Specifies the value of the Codt field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the EmpresaVacunadora field.';
                    ApplicationArea = All;
                }
                field(FechaIniPlanificacion; Rec.FechaIniPlanificacion)
                {
                    ToolTip = 'Specifies the value of the FechaIniPlanificacion field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    ToolTip = 'Aquí aparecerá el nombre de la empresa de vacuna';
                    Caption = 'Nombre de la Empresa vacunadora';
                    ApplicationArea = All;
                }

            }
        }
    }

    /// <summary>
    /// Funcion para 
    /// </summary>
    /// <param name="xSalida">VAR Record MXCPllanVacunacionC01.</param>
    procedure GetSelectionFliterF(var xSalida: Record MXCPllanVacunacionC01)
    begin
        CurrPage.SetSelectionFilter(xSalida);//esto que hace la selecion que ha hecho el usuario

    end;
}
