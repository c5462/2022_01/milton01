/// <summary>
/// la pagina de configuracion de la pagina de nuestra app
/// 
/// </summary>
page 50000 "MXCConfiguracionC01"
{
    Caption = 'Configuración de la app del Curso01';
    PageType = Card;
    SourceTable = MXCConfiguracionC01;
    AboutTitle = 'En esta página crearemos la configuracion de la  primera app del curso de Especilistas de Bussines Central 2022';
    AboutText = 'Congigure la app de forma correcta. Asegurase que asigna el valor del campo cleinte web';
    AdditionalSearchTerms = 'Mi primera App';
    UsageCategory = Administration;
    ApplicationArea = All;
    DeleteAllowed = false;
    InsertAllowed = false;
    ShowFilter = true;//para ver los filtros en la pag
    SaveValues = true;//SaveValues=true , lo ultimo que has puesto se lo mantiene durante 
    /*
    -AboutTitle
    -AboutText
    -AdditionalSearchTerms
    -PopulateAllFields cuando necesitamos que se rellenes los campos, en los sublist    
    */

    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Codigo de la Tabla';
                    ApplicationArea = All;
                }
                /*
              //usando Snippets de Wlado
                field(MyField; Rec.Id)
                {
                    ApplicationArea = All;
                }
                */
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web.';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Code. cliente web';
                    AboutText = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web';
                    Importance = Promoted;
                    //AssistEdit=true;
                    //ShowMandatory hace que muestre la  advertencia
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                    AboutText = 'ES un texto registro en el que puedes hacer maravillas ';
                    AboutTitle = 'Es lo mismo de antes pero con AboutTitl';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo de Texto field.';
                    ApplicationArea = All;
                }
                field(FondoColor; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the Color fondo field.';
                    ApplicationArea = All;
                }
                field(Fondo; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the Color fondo field.';
                    ApplicationArea = All;
                }
                field(password; xpasword)
                {
                    Caption = 'Password';

                    ToolTip = 'Contraseña Actual';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de tabla field.';
                    ApplicationArea = All;
                }

                field(NombreTabla; Rec.NombreTablaF(Rec.TipoTabla, Rec.CodTabla))
                {
                    Caption = 'Nombre de la tabla';
                    ToolTip = 'Specifies the value of the Tipo de tabla field.';
                    ApplicationArea = All;
                }
                field(Numerounidadesvendidas; Rec.UdsDisponibles)
                {
                    ToolTip = 'Specifies the value of the Nº de unidades vendidas field.';
                    ApplicationArea = All;
                }
                field(xNumIteraciones; xNumIteracion)
                {
                    Caption = 'Numero de itercones';
                    ApplicationArea = All;
                }


            }

            group(InfInterna)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                    Importance = Additional;

                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(CamposCalculados)
            {
                Caption = 'campo calculado';

                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Specifies the value of the Nombre del cliente  field.';
                    ApplicationArea = All;
                    //llamaremos a la funcion que se hizo en la tabla
                }
                field(NombreClientePorFuncion; Rec.NombreClienteF(Rec.CodCteWeb))
                {
                    Caption = 'Nombre del cliente';
                    ApplicationArea = All;
                }
            }



        }
    }
    actions
    {
        area(Processing)
        {
            ///Name : nada de caracteres especiales, a-z y 0-9
            action(Ejemplo01)
            {
                ApplicationArea = All;
                Caption = 'Mensaje de acción.';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Accion pulsada');
                end;

            }

            action(PedirDatos)
            {
                Caption = 'Pedir datos Varios';
                ApplicationArea = All;
                trigger OnAction()
                var
                    pglMXCEntradaDeDatosVariosC01: Page MXCEntradaDeDatosVariosC01;
                    xlBool: Boolean;
                    xlDate: Date;
                    xlDec: Decimal;
                begin
                    //pglMXCEntradaDeDatosVariosC01.Run();//si solo se hace hace no capturamos la acion
                    pglMXCEntradaDeDatosVariosC01.CampoF('Ciudad de naimiento', 'Albacete');
                    pglMXCEntradaDeDatosVariosC01.CampoF('Pais de Nacimiento', '');
                    pglMXCEntradaDeDatosVariosC01.CampoF('Estado ', 'CLM');
                    pglMXCEntradaDeDatosVariosC01.CampoF('Cuota Pagada 1º Trimestre', false);
                    pglMXCEntradaDeDatosVariosC01.CampoF('Cuota Pagada 2º Trimestre', true);
                    pglMXCEntradaDeDatosVariosC01.CampoF('Campo Decimal', 0);
                    pglMXCEntradaDeDatosVariosC01.CampoF('Importe 2', 10.78);
                    pglMXCEntradaDeDatosVariosC01.CampoF('Importe 3', 18.4);
                    pglMXCEntradaDeDatosVariosC01.CampoF('Fecha 1', 0D);
                    pglMXCEntradaDeDatosVariosC01.CampoF('Fecha 2', Today);

                    if pglMXCEntradaDeDatosVariosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
                        Message(pglMXCEntradaDeDatosVariosC01.CampoF());
                        Message(pglMXCEntradaDeDatosVariosC01.CampoF());
                        Message(pglMXCEntradaDeDatosVariosC01.CampoF());
                        Message('Couta 1 %1', pglMXCEntradaDeDatosVariosC01.CampoF(xlBool));
                        pglMXCEntradaDeDatosVariosC01.CampoF(xlBool);
                        Message('Couta 2 %1', xlBool);
                        pglMXCEntradaDeDatosVariosC01.CampoF(xlDec);
                        Message('Importe 01 %1', xlDec);
                        pglMXCEntradaDeDatosVariosC01.CampoF(xlDec);
                        Message('Importe 02 %1', xlDec);
                        Message('Importe 03 %1', pglMXCEntradaDeDatosVariosC01.CampoF(xlDec, 3));
                        pglMXCEntradaDeDatosVariosC01.CampoF(xlDate);
                        Message('La Fecha es %1', xlDate);
                    end else begin
                        Error('Proceso Cancelado por el usuario');

                    end;

                end;
            }
            action(CambiarContraseña)
            {
                Caption = 'Cambiar password';
                ApplicationArea = All;
                trigger OnAction()
                begin
                    CambiarPassword();
                end;
            }
            action(ExportarVacunas)
            {
                Caption = 'Exportar Vacunas ';
                ApplicationArea = all;
                trigger OnAction()
                begin
                    ExportarVacunasF();
                end;
            }
            action(ListaFiltrada)
            {
                Caption = 'lista filtrada clientes Cod.alm:Gris';
                ApplicationArea = all;

                trigger OnAction()

                begin
                    ListarCLientes();
                end;

            }
            action(ImportarVacunas)
            {
                Caption = 'Importar Vacunas ';
                ApplicationArea = all;
                trigger OnAction()
                begin
                    ImportarVacunasF();
                end;
            }
            action(MXCTextC01)
            {
                ApplicationArea = All;
                Caption = ' Puebas con Text';
                trigger OnAction()
                var
                    xlInicion: DateTime;
                    i: Integer;
                    xlText: Text;
                begin
                    xlInicion := CurrentDateTime;
                    for i := 1 to xNumIteracion do begin
                        xlText += '.';

                    end;
                    Message('Tiempo invertido (%1) con texto: %2', CurrentDateTime - xlInicion, xlText);
                end;
            }
            action(MXCTextViewC01)
            {
                ApplicationArea = All;
                Caption = 'Texto Con TextBuilder';
                trigger OnAction()
                var
                    xlInicion: DateTime;
                    i: Integer;
                    xlText: TextBuilder;
                begin
                    xlInicion := CurrentDateTime;
                    for i := 1 to xNumIteracion do begin
                        xlText.Append('.');

                    end;
                    Message('Tiempo invertido (%1): con texto: %2', CurrentDateTime - xlInicion, xlText.ToText());
                end;
            }

            action(MXCContarCtes)
            {
                ApplicationArea = All;
                Caption = 'Contar Clientes';
                trigger OnAction()
                var
                    culMXCFuncionesAppcC01: Codeunit MXCFuncionesAppcC01;
                begin
                    culMXCFuncionesAppcC01.UsodeListasF();
                end;
            }

            action(MXCESTAFUNCIONC01)
            {
                Caption = 'Funcion para ver que un registro esta dentro de un filtro';
                Image = Allocations;
                ApplicationArea = all;

                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    culMXCFuncionesAppcC01: Codeunit MXCFuncionesAppcC01;
                begin
                    culMXCFuncionesAppcC01.DentroDeUnFiltroF(rlCustomer."No.", 'C00..C999');
                end;


            }

            action(MXCPantallaC01)
            {
                Caption = 'Pantalla registro';
                ApplicationArea = all;
                Image = AgreementQuote;
                trigger OnAction()
                var
                // rlCompany: Record Company;
                // rlCustomer: Record Customer;
                // pglCustomerList: Page "Customer List";
                // xlFiltroGrp: Integer;
                begin
                    Message('hola');
                end;
            }





        }
    }

    trigger OnOpenPage()
    var
        xChar: Char;
        xCharM: Char;

    begin
        Rec.GetF();
        xChar := 380;//las z con punto encima;
        xCharM := 379;
        Message('%1 y su may:%2', xChar, xCharM);

    end;

    [NonDebuggable]
    local procedure CambiarPassword()
    var
        pglMXCEntradaDeDatosVariosC01: Page MXCEntradaDeDatosVariosC01;
        xlPass: Text;
    begin
        // Rec.Modify(true);
        // Commit();
        pglMXCEntradaDeDatosVariosC01.CampoF('Password Actual', '', true);
        pglMXCEntradaDeDatosVariosC01.CampoF('Nueva Password', '', true);
        pglMXCEntradaDeDatosVariosC01.CampoF('Confirma Nueva Password', '', true);
        if pglMXCEntradaDeDatosVariosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            // if pglMXCEntradaDeDatosVariosC01.CampoF(xlPass) = xpasword then begin
            if pglMXCEntradaDeDatosVariosC01.CampoF(xlPass) = Rec.PasswordF() then begin
                if pglMXCEntradaDeDatosVariosC01.CampoF(xlPass) = pglMXCEntradaDeDatosVariosC01.CampoF(xlPass) then begin//pero devulven valores distintos
                    //xpasword := xlPass;
                    Rec.PasswordF(xlPass);
                    Message('Password Cambiado');
                end else begin
                    Error('Nuevo password no coincide');
                end;
            end else begin
                Error('password actual no coincide');
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;

    local procedure ExportarVacunasF()
    var
        //Para las lineas 
        rlMXCLinPlanVacunacionMXCC01: Record "MXCLinPlanVacunacionC01";
        //tipo re de nuestra cabecera de vacunas
        rlMXCPllanVacunacionC01: Record MXCPllanVacunacionC01;
        TempLMXCPllanVacunacionC01: Record MXCPllanVacunacionC01 temporary;
        pglMXCListaPLanVacunacionC01: Page MXCListaPLanVacunacionC01;
        //pglMXCSubPageLinPlanVacunacioC01: Page MXCSubPageLinPlanVacunacioC01;

        XlInStr: Instream;
        ClCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
        ClCadenaLinea: Label 'L%1%2%1%3%1%4%1%5', Locked = true;
        ClSeparador: Label '.', Locked = true;
        ClSeparadorLinea: Label '-', Locked = true;
        xlOutStr: OutStream;
        //  xlinea: Text;
        xlNombreFichero: Text;
    begin
        TempLMXCPllanVacunacionC01.MiBlob.CreateOutStream(xlOutStr, TextEncoding::Windows);
        Message('Seleciona vacunas a exportar');
        pglMXCListaPLanVacunacionC01.LookupMode(true);//si quiero que nos aparezca el acepta y cancelar
        pglMXCListaPLanVacunacionC01.SetRecord(rlMXCPllanVacunacionC01);//comunicar con la pagina porque sino no coge los registros
        /*Page.RunModal(0,rlMXCPllanVacunacionC01) //esto si queremos un registro solo
        pglMXCSubPageLinPlanVacunacioC01.SetRecord(rlMXCLinPlanVacunacionMXCC01);//hcemos la comunicacion con la pagina*/
        if pglMXCListaPLanVacunacionC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            //dame el dataset , el grupo de resgistro
            //Message('%1',pglMXCListaPLanVacunacionC01.GetSelectionFliterF().Count);//esto me contaria las veces que he seleccionado 
            pglMXCListaPLanVacunacionC01.GetSelectionFliterF(rlMXCPllanVacunacionC01);

            //if rlMXCPllanVacunacionC01.FindFirst() then begin//me trae el dataset
            if rlMXCPllanVacunacionC01.FindSet(false) then begin
                ///no tiene parametros , si no lo vamos a modificar false ,diferencia en tiempode procesamiento... en caso de modificar (campos y clave) =TRUE;TRUE
                repeat
                    Message('%1', rlMXCPllanVacunacionC01);
                    xlOutStr.WriteText(StrSubstNo(ClCadenaCabecera, ClSeparador, rlMXCPllanVacunacionC01.CodCabeceras, rlMXCPllanVacunacionC01.Descripcion, rlMXCPllanVacunacionC01.EmpresaVacunadora, rlMXCPllanVacunacionC01.FechaIniPlanificacion));
                    xlOutStr.WriteText();
                    //aplicando filtros de la linea
                    rlMXCLinPlanVacunacionMXCC01.SetRange(CodLineas, rlMXCPllanVacunacionC01.CodCabeceras);
                    if rlMXCLinPlanVacunacionMXCC01.FindSet(false) then begin

                        repeat
                            xlOutStr.WriteText(StrSubstNo(ClCadenaLinea, ClSeparadorLinea, rlMXCLinPlanVacunacionMXCC01.NumLinea, rlMXCLinPlanVacunacionMXCC01.ClientetoVacuna, rlMXCLinPlanVacunacionMXCC01.FechaVacunacion, rlMXCLinPlanVacunacionMXCC01.Fecha2Vacunacion, rlMXCLinPlanVacunacionMXCC01.NombreClientetoVacunaF(), rlMXCLinPlanVacunacionMXCC01.ClientetoVacuna));
                            xlOutStr.WriteText();
                        until rlMXCLinPlanVacunacionMXCC01.Next() = 0;
                    end;
                until rlMXCPllanVacunacionC01.Next() = 0;
            end else begin
                Error('Proceso terminado por el usuario');
            end;
        end;
        TempLMXCPllanVacunacionC01.MiBlob.CreateInStream(XlInStr);
        //xlNombreFichero := 'Cabecera.csv';
        if not DownloadFromStream(xlInStr, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;
    end;

    local procedure ListarClientes()
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
        xlFiltroGrp: Integer;
    begin
        //Mostrar la lista de empresa de la bbdd y de la selececionada mostrar sus clientes cod.almacen =GRIS
        if AccionAceptadaF(Page.RunModal(Page::Companies, rlCompany)) then begin
            rlCustomer.ChangeCompany(rlCompany.Name);
            xlFiltroGrp := rlCustomer.FilterGroup();//guardamos el filtro de grupo en el que estamos        
            rlCustomer.FilterGroup(xlFiltroGrp + 20);//filtado por 
            rlCustomer.SetRange("Location Code", 'GRIS');//filtramos por lo que queremos
            rlCustomer.FilterGroup(xlFiltroGrp);//vuelve  
            if AccionAceptadaF(Page.RunModal(Page::"Customer List", rlCustomer)) then begin
                Message('TODO OK');
            end else begin
                Error('error');
            end;
        end else begin
            Error('Proceso cancelado por el uuario');
        end;
    end;

    local procedure ImportarVacunasF()
    var
        TempLMXCConfiguracionC01: Record MXCConfiguracionC01 temporary;
        rlMXCLinPlanVacunacionMXCC01: Record "MXCLinPlanVacunacionC01";
        /*
            //tipo re de nuestra cabecera de vacunas
            rlMXCPllanVacunacionC01: Record MXCPllanVacunacionC01;
            pglMXCListaPLanVacunacionC01: Page MXCListaPLanVacunacionC01;
            TempLMXCPllanVacunacionC01: Record MXCPllanVacunacionC01 temporary;
            ClCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
            ClCadenaLinea: Label 'L%1%2%1%3%1%4%1%5', Locked = true;
            ClSeparador: Label '.', Locked = true;
            ClSeparadorLinea: Label '-', Locked = true;
            //Para las lineas b
            rlMXCLinPlanVacunacionMXCC01: Record MXCLinPlanVacunacionMXCC01;
            pglMXCSubPageLinPlanVacunacioC01: Page MXCSubPageLinPlanVacunacioC01;
    */
        rlMXCPllanVacunacionC01: Record MXCPllanVacunacionC01;
        xlFecha: Date;
        XlInStr: Instream;
        xNumLinea: Integer;
        ClSeparador: Label '.', Locked = true;
        ClSeparadorLinea: Label '-', Locked = true;
        xlinea: Text;
    begin
        TempLMXCConfiguracionC01.MiBlob.CreateInStream(XlInStr, TextEncoding::Windows);
        if UploadIntoStream('', XlInStr) then begin
            while not XlInStr.EOS do begin //mientras que no hayamos llegado al fin del string EOS_> final del string
                XlInStr.ReadText(xlinea);//leemos la linea, 
                Message(xlinea);
                //Insertar Cabecera o lineas en funcion del 1er caracter de la line
                case true of
                    xlinea[1] = 'C':  //Alta Cabecera
                                      //help
                                      //xlOutStr.WriteText(StrSubstNo(ClCadenaCabecera, ClSeparador, rlMXCPllanVacunacionC01.CodCabeceras, rlMXCPllanVacunacionC01.Descripcion, rlMXCPllanVacunacionC01.EmpresaVacunadora, rlMXCPllanVacunacionC01.FechaIniPlanificacion));
                        begin//insertar registros
                            rlMXCPllanVacunacionC01.Init();
                            rlMXCPllanVacunacionC01.Validate(CodCabeceras, xlinea.Split(ClSeparador).Get(2));
                            rlMXCPllanVacunacionC01.Insert(true);
                            rlMXCPllanVacunacionC01.Validate(CodCabeceras, xlinea.Split(ClSeparador).Get(2));
                            rlMXCPllanVacunacionC01.Validate(Descripcion, xlinea.Split(ClSeparador).Get(3));
                            rlMXCPllanVacunacionC01.Validate(EmpresaVacunadora, xlinea.Split(ClSeparador).Get(4));
                            if Evaluate(xlFecha, xlinea.Split(ClSeparador).Get(5)) then begin
                                rlMXCPllanVacunacionC01.Validate(FechaIniPlanificacion, xlFecha);
                            end;
                            rlMXCPllanVacunacionC01.Modify(true);
                        end;

                    xlinea[1] = 'L':
                        //ALta linea
                        //  xlOutStr.WriteText(StrSubstNo(ClCadenaLinea, ClSeparadorLinea, rlMXCLinPlanVacunacionMXCC01.NumLinea, rlMXCLinPlanVacunacionMXCC01.ClientetoVacuna, rlMXCLinPlanVacunacionMXCC01.FechaVacunacion, rlMXCLinPlanVacunacionMXCC01.Fecha2Vacunacion, rlMXCLinPlanVacunacionMXCC01.NombreClientetoVacunaF(), rlMXCLinPlanVacunacionMXCC01.ClientetoVacuna));
                        begin
                            rlMXCLinPlanVacunacionMXCC01.Init();
                            rlMXCLinPlanVacunacionMXCC01.Validate(CodLineas, xlinea.Split(ClSeparadorLinea).Get(2));
                            if Evaluate(xNumLinea, xlinea.Split(ClSeparadorLinea).Get(2)) then begin
                                rlMXCLinPlanVacunacionMXCC01.Validate(NumLinea, xNumLinea);

                            end;

                            rlMXCLinPlanVacunacionMXCC01.Insert(true);
                            rlMXCLinPlanVacunacionMXCC01.Validate(ClientetoVacuna, xlinea.Split(ClSeparadorLinea).Get(3));
                            rlMXCLinPlanVacunacionMXCC01.Validate(ClientetoVacuna, xlinea.Split(ClSeparadorLinea).Get(3));
                            if Evaluate(xlFecha, xlinea.Split(ClSeparadorLinea).Get(4)) then begin
                                rlMXCLinPlanVacunacionMXCC01.Validate(FechaVacunacion, xlFecha);
                            end;
                            if Evaluate(xlFecha, xlinea.Split(ClSeparadorLinea).Get(5)) then begin
                                rlMXCLinPlanVacunacionMXCC01.Validate(Fecha2Vacunacion, xlFecha);
                            end;

                            rlMXCLinPlanVacunacionMXCC01.Modify(true);
                        end;

                    else
                        Error('TPO de linea %1 no reconocido', xlinea[1]);
                end;
            end;
        end else begin
            Error('No se ha podido conectat con el servidor');
        end;
    end;

    local procedure AccionAceptadaF(pAccion: Action) returnValue: Boolean
    begin
        returnValue := pAccion in [Action::LookupOK, Action::OK, Action::Yes];
    end;


    local procedure ValorCampoF(): Text

    begin

    end;



    var
        xNumIteracion:
                Integer;
        xpasword: Text;
}
