/// <summary>
/// Configuracon2
/// </summary>
page 50003 "MXCConfiguracioin2C01"
{
    ApplicationArea = All;
    Caption = ' lista Configuracioin2C01';
    PageType = List;
    SourceTable = MXCConfiguracionC01;
    UsageCategory = Lists;
    ShowFilter = true;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Cod2; Rec.Cod2)
                {
                    ToolTip = 'Specifies the value of the Código 2 field.';
                    ApplicationArea = All;
                }
                field(CodCteWeb; Rec.CodCteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por web.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    ToolTip = 'Specifies the value of the Color fondo field.';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    ToolTip = 'Specifies the value of the Color fondo field.';
                    ApplicationArea = All;
                }
                field(Id; Rec.Id)
                {
                    ToolTip = 'Specifies the value of the Id de registro field.';
                    ApplicationArea = All;
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo de Texto field.';
                    ApplicationArea = All;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tipo de tabla field.';
                    ApplicationArea = All;
                }
                field(UdsDisponibles; Rec.UdsDisponibles)
                {
                    ToolTip = 'Specifies the value of the Nº de unidades vendidas field.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreCliente)
                {
                    ToolTip = 'Specifies the value of the Nombre del cliente  field.';
                    ApplicationArea = All;
                }
                field(TextoRegistro; Rec.TextoRegistro)
                {
                    ToolTip = 'Specifies the value of the Texto registro field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
