/// <summary>
/// Ficha de la cabecera del plan de vacunacion
/// </summary>
page 50004 "MXCFichaPLanVacunacionC01"
{
    Caption = 'FichaPLanVacunacionC01';
    PageType = Document;
    SourceTable = MXCPllanVacunacionC01;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Codt; Rec.CodCabeceras)
                {
                    ToolTip = 'Specifies the value of the Codt field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripcion field.';
                    ApplicationArea = All;
                }
                field(FechaIniPlanificacion; Rec.FechaIniPlanificacion)
                {
                    ToolTip = 'Specifies the value of the FechaIniPlanificacion field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the EmpresaVacunadora field.';
                    ApplicationArea = All;
                }
                field(nombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'MyField';
                    ApplicationArea = All;
                    //DataClassification = ToBeClassified;
                    //llamar al tolltip de

                }


            }
            //part (Nombre que queramos)
            part(Lineas; MXCSubPageLinPlanVacunacioC01)
            {
                ApplicationArea = all;
                SubPageLink = CodLineas = field(CodCabeceras);
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(EjemploVar01)
            {
                Caption = 'Ejemplo de Variables 01';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culMXCFuncionesAppcC01: Codeunit MXCFuncionesAppcC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'Milton modificando para entender texto sin var';
                    culMXCFuncionesAppcC01.EjmVariablesE_SF(xlTexto);
                    Message(xlTexto);
                end;
            }

            action(EjemploVar02)
            {
                Caption = 'Ejemplo de Variables 01';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culMXCFuncionesAppcC01: Codeunit MXCFuncionesAppcC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('Milton modificando para entender texto sin var');
                    culMXCFuncionesAppcC01.EjmVariablesE_SF02(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(Bingo)
            {
                Caption = 'Bingo ';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culMXCFuncionesAppcC01: Codeunit MXCFuncionesAppcC01;
                begin
                    culMXCFuncionesAppcC01.EjemploArrayF();
                end;
            }
            action(PriebaFucSegPlano)
            {
                Caption = 'Funcion en Segundo PLano ';
                ApplicationArea = All;

                trigger OnAction()
                // var
                //     culMXCFuncionesAppcC01: Codeunit MXCFuncionesAppcC01;

                var
                    xlSesionIniciada: Integer;
                begin

                    // culMXCFuncionesAppcC01.EjemploArrayF();

                    //Otra forma de hacrlo  se utiliaz mucho en paginas y
                    //Codeunit.Run(Codeunit::MXCCodeUnitLentaC01);
                    //------------------------------------------
                    //hacerlo de forma que trabaje en segundo plano 

                    StartSession(xlSesionIniciada, Codeunit::MXCCodeUnitLentaC01);
                    Message('Proceso lanzado en segundo Plano');

                end;
            }
            action(EnPrimerPlano)
            {
                Caption = 'Funcion Ejecutar tarea F';
                Image = "8ball";
                ApplicationArea = All;

                trigger OnAction()
                begin

                    // culMXCFuncionesAppcC01.EjemploArrayF();

                    //Otra forma de hacrlo  se utiliaz mucho en paginas y
                    Codeunit.Run(Codeunit::MXCCodeUnitLentaC01);
                    //------------------------------------------
                    //hacerlo de forma que trabaje en segundo plano 

                    //StartSession(xlSesionIniciada, Codeunit::MXCCodeUnitLentaC01);
                    // Message('Proceso lanzado en segundo Plano');

                end;
            }



            action(Logs)
            {
                ApplicationArea = all;
                Caption = 'Logs';
                Image = Error;
                RunObject = page MXCLogsC01;

            }
            action(CrearLineas)
            {
                ApplicationArea = all;
                Caption = 'Crear líneas ';
                Image = Create;
                //RunObject = page MXCLogsC01;
                trigger OnAction()
                var
                    //vamos a guardasr tiempos
                    rlMXCLinPlanVacunacionMXCC01: Record "MXCLinPlanVacunacionC01";
                    xlInicio: DateTime;
                    i: Integer;
                begin
                    xlInicio := CurrentDateTime;
                    for i := 1 to 10 do begin
                        //crear liness insear registos
                        rlMXCLinPlanVacunacionMXCC01.Init();//limpia 
                        rlMXCLinPlanVacunacionMXCC01.Validate(CodLineas, Rec.CodCabeceras);
                        rlMXCLinPlanVacunacionMXCC01.Validate(NumLinea, i);
                        rlMXCLinPlanVacunacionMXCC01.Insert(true);
                        rlMXCLinPlanVacunacionMXCC01.Modify(true);
                    end;

                    Message('%1', CurrentDateTime - xlInicio);
                end;

            }

            action(EjecucionNOBLUCK)
            {
                ApplicationArea = all;
                Caption = 'NO bulck ';
                Image = Create;
                //RunObject = page MXCLogsC01;
                trigger OnAction()
                var
                    //vamos a guardasr tiempos
                    rlMXCLinPlanVacunacionMXCC01: Record "MXCLinPlanVacunacionC01";
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlMXCLinPlanVacunacionMXCC01.SetRange(CodLineas, rec.CodCabeceras);
                    //1 si mdificamos 
                    if rlMXCLinPlanVacunacionMXCC01.FindSet(true, false) then begin
                        repeat
                            rlMXCLinPlanVacunacionMXCC01.Validate(FechaVacunacion, Today + 1);
                            rlMXCLinPlanVacunacionMXCC01.Modify(true)
                        until rlMXCLinPlanVacunacionMXCC01.Next() = 0;
                    end;


                    Message('%1', CurrentDateTime - xlInicio);
                end;

            }

            action(CREarFEchaMAsi)
            {
                ApplicationArea = all;
                Caption = 'Crear lineas de fecha bulck ';
                Image = Create;
                //RunObject = page MXCLogsC01;
                trigger OnAction()
                var
                    //vamos a guardasr tiempos
                    rlMXCLinPlanVacunacionMXCC01: Record "MXCLinPlanVacunacionC01";
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime;
                    rlMXCLinPlanVacunacionMXCC01.SetRange(CodLineas, rec.CodCabeceras);
                    rlMXCLinPlanVacunacionMXCC01.ModifyAll(FechaVacunacion, Today, true);

                    Message('%1', CurrentDateTime - xlInicio);
                end;

            }
        }
    }
}
